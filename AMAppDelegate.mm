#import <NinevehGL/NinevehGL.h>
#import "AMAppDelegate.h"

#import "AMRootViewController.h"
#import "iOSHierarchyViewer.h"
#import "AMObjectionModule.h"
#import "JSObjection.h"

@implementation AMAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    AMObjectionModule *objectionModule = [AMObjectionModule module];
    JSObjectionInjector *defaultInjector = [JSObjection createInjector:objectionModule];
    [JSObjection setDefaultInjector:defaultInjector];

    nglGlobalTextureQuality(NGLTextureQualityTrilinear);
    nglGlobalAntialias(NGLAntialias4X);
    nglGlobalTextureOptimize(NGLTextureOptimizeNone);
    nglGlobalColorFormat(NGLColorFormatRGBA);
    nglGlobalFlush();

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    self.window.rootViewController = defaultInjector[[AMRootViewController class]];
    [self.window makeKeyAndVisible];

    [iOSHierarchyViewer start];

    return YES;
}

@end
