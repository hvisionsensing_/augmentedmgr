//
//  Created by Michał Mizera on 17.04.2013.
//


#import <Foundation/Foundation.h>

@class AMMarkersMapView;


@interface AMRootView : UIView
@property(nonatomic, strong) UIImageView *cameraImage;
@property(nonatomic, strong) UIButton *createMapButton;
@property(nonatomic, strong) UIButton *calibrateButton;
@property(nonatomic, strong) UIButton *previewMapButton;
@property(nonatomic, strong) UIButton *saveMapButton;
@property(nonatomic, strong) UIButton *downloadMapButton;
@property(nonatomic, strong) UIButton *uploadMapButton;
@property(nonatomic, strong) UILabel *markersMapSizeLabel;
@property(nonatomic, strong) UIButton *gameplayButton;

- (void)setMarkersSize:(NSInteger)size;

- (void)setNetworkButtonActive:(BOOL)active;
@end
