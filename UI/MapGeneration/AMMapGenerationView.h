//
//  Created by Michał Mizera on 08.10.2013.
//


#import <Foundation/Foundation.h>

@class AMMarkersMapView;
@class AMMapPreview3DOverlayView;
@class AMMarkersMap;


@interface AMMapGenerationView : UIView <UIScrollViewDelegate>
@property(nonatomic, strong) UIImageView *cameraImage;
@property(nonatomic, strong) UIImageView *undistortedImage;

@property(nonatomic, strong) UILabel *debugLabel;
@property(nonatomic, strong) UIImageView *canonicalMarkerImage;
@property(nonatomic, strong) UILabel *lastErrorLabel;
@property(nonatomic, strong) AMMarkersMapView *markersMapView;
@property(nonatomic, strong) UIButton *resetButton;
@property(nonatomic, strong) UIButton *backButton;
@property(nonatomic, strong) UIScrollView *mapViewScrollView;

- (id)initWithFrame:(CGRect)frame markersMap:(AMMarkersMap *)markersMap;

- (void)scrollToCenter;
@end
