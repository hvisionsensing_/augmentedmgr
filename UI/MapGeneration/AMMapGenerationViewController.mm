//
//  Created by Michał Mizera on 08.10.2013.
//


#import <Objection/Objection.h>
#import <NinevehGL/NinevehGL.h>
#import "AMMapGenerationViewController.h"
#import "AMMarker.h"
#import "AMArEngine.h"
#import "AMMapGenerationView.h"
#import "AMMarkersMapView.h"
#import "core.hpp"
#import "UIImage+OpenCV.h"
#import "AMMapPreview3DOverlayView.h"
#import "AMMarkersDetector.h"
#import "AMUserDefaultsManager.h"
#import "AMMarkersMap.h"

@interface AMMapGenerationViewController ()
@property(nonatomic, strong) AMArEngine *arEngine;
@property(nonatomic, strong) AMUserDefaultsManager *userDefaultsManager;
@end

NSInteger const kCenterMarkerId = 29;

@implementation AMMapGenerationViewController {
}

objection_requires_sel(@selector(arEngine), @selector(userDefaultsManager))

- (AMMapGenerationView *)castView {
    return (AMMapGenerationView *) self.view;
}

- (void)loadView {
    self.view = [[AMMapGenerationView alloc] initWithFrame:CGRectZero markersMap:_userDefaultsManager.markersMap];

    [self.castView.backButton addTarget:self action:@selector(backButtonTapped)
                       forControlEvents:UIControlEventTouchUpInside];
    [self.castView.resetButton addTarget:self action:@selector(resetButtonTapped)
                        forControlEvents:UIControlEventTouchUpInside];

    self.castView.markersMapView.delegate = self;
}

- (void)backButtonTapped {
    [_arEngine removeObserver:self];

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)resetButtonTapped {
    [_userDefaultsManager clearMarkers];
    [self.castView.markersMapView resetAllMarkers];
    self.castView.canonicalMarkerImage.image = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.castView scrollToCenter];
    _arEngine.mode = AMArEngineModeMapGeneration;
    [_arEngine addObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    _arEngine.mode = AMArEngineModeDisabled;
    [_arEngine removeObserver:self];
}


- (void)nextFrameReady {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.castView.cameraImage.image = [UIImage imageWithCVMat:*(_arEngine.image)];

        if (_arEngine.undistortedImage) {
            self.castView.undistortedImage.image = [UIImage imageWithCVMat:*(_arEngine.undistortedImage)];
        }

        NSDictionary *currentMarkersDict = [_arEngine markersDict];

        [self analyzeMarkers:currentMarkersDict];
        [self.castView.markersMapView updateWithMarkersMap:_userDefaultsManager.markersMap centerMarkerId:kCenterMarkerId];
    });
}

- (void)analyzeMarkers:(NSDictionary *)currentMarkers {
    for (AMMarker *marker in [currentMarkers allValues]) {
        [self setupMarker:marker.id allMarkers:currentMarkers];
    }
}

- (void)setupMarker:(NSInteger)markerId allMarkers:(NSDictionary *)allMarkers {
    BOOL markerExistsInMap = _userDefaultsManager.markersMap[@(markerId)] != nil;
    BOOL markerHasFixedPosition = ((AMMarker *) _userDefaultsManager.markersMap[@(markerId)]).fixPosition;
    if (markerExistsInMap && markerHasFixedPosition) return;

    AMMarker *currentMarker = allMarkers[@(markerId)];

    AMMarker *centerMarkerInCurrentDict = allMarkers[@(kCenterMarkerId)];
    AMMarker *centerMarker = _userDefaultsManager.markersMap[@(kCenterMarkerId)];

    BOOL currentMarkersContainsCentralMarker = centerMarkerInCurrentDict != nil;
    if (currentMarkersContainsCentralMarker) {
        // setup directly from center

        [self normalizeMarker:&currentMarker toMarker:centerMarkerInCurrentDict];

        _userDefaultsManager.markersMap[@(currentMarker.id)] = currentMarker;
    } else {
        // localizing marker indirectly
        for (AMMarker *anchorMarker in allMarkers.allValues) {
            AMMarker *anchorMarkerInSavedMap = _userDefaultsManager.markersMap[@(anchorMarker.id)];

            BOOL mapContainsMarker = anchorMarker != nil;
            BOOL anchorMarkerIsCurrentMarker = anchorMarker.id == markerId;
            BOOL anchorHasFixedSize = anchorMarkerInSavedMap.fixPosition;
            if (mapContainsMarker && !anchorMarkerIsCurrentMarker && anchorHasFixedSize) {

                [self normalizeMarker:&currentMarker toMarker:anchorMarker];

                [currentMarker rotate:-anchorMarkerInSavedMap.rotation pivot:centerMarker.center];
                currentMarker.center += anchorMarkerInSavedMap.center;

                _userDefaultsManager.markersMap[@(currentMarker.id)] = currentMarker;

                break;
            }
        }
    }
}

- (void)normalizeMarker:(AMMarker **)marker toMarker:(AMMarker *)anchorMarker {
    [*marker rotate:anchorMarker.rotation pivot:anchorMarker.center];
    cv::Point2f center = (*marker).center - anchorMarker.center;
    center.x /= anchorMarker.size;
    center.y /= anchorMarker.size;
    (*marker).center = center;
}

- (void)markerViewButtonTapped:(NSInteger)markerId {
    AMMarker *marker = _userDefaultsManager.markersMap[@(markerId)];
    marker.fixPosition = !marker.fixPosition;
}

@end
