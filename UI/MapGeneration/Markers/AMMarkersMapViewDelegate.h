//
//  Created by Michał Mizera on 20.04.2013.
//


#import <Foundation/Foundation.h>

@protocol AMMarkersMapViewDelegate <NSObject>
- (void)markerViewButtonTapped:(NSInteger)markerId;
@end
