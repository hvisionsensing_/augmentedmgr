//
//  Created by Michał Mizera on 19.04.2013.
//


#import <CoreGraphics/CoreGraphics.h>
#import "AMMarkersMapView.h"
#import "AMMarkerView.h"
#import "AMMarker.h"
#import "AMMarkersMapViewDelegate.h"
#import "AMMarkersMap.h"


NSInteger kMarkerViewSize = 50;

@implementation AMMarkersMapView {
    NSMutableDictionary *_markersViewsDict;
    AMMarkersMap *markersMap;
}

- (id)initWithFrame:(CGRect)frame markersMap:(AMMarkersMap *)aMarkersMap {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupMarkersViewsWithMap:aMarkersMap];

        self.clipsToBounds = NO;
    }

    return self;
}

- (void)setupMarkersViewsWithMap:(AMMarkersMap *)aMarkersMap {
    _markersViewsDict = [NSMutableDictionary dictionary];

    for (int i = 0; i < 32; i++) {
        AMMarkerView *markerView = [[AMMarkerView alloc] initWithFrame:CGRectMake(0, 0, kMarkerViewSize, kMarkerViewSize)];
        [markerView setMarkerId:i];
        markerView.markerButton.tag = i;
        [markerView.markerButton addTarget:self action:@selector(markerViewButtonClicked:)
                          forControlEvents:UIControlEventTouchUpInside];
        _markersViewsDict[@(i)] = markerView;
        AMMarker *marker = aMarkersMap[@(i)];

        if (marker && marker.fixPosition) {
            [markerView setMarkerAsFixed:YES];
        }

        [self addSubview:markerView];
    }
}

- (void)resetAllMarkers {
    for (AMMarkerView *markerView in _markersViewsDict.allValues) {
        [markerView setMarkerAsFixed:NO];
    }
}

- (void)markerViewButtonClicked:(UIButton *)markerButton {
    if ([_delegate respondsToSelector:@selector(markerViewButtonTapped:)]) {
        NSInteger markerId = markerButton.tag;
        [_delegate markerViewButtonTapped:markerId];
        AMMarkerView *markerView = _markersViewsDict[@(markerId)];
        [markerView setMarkerAsFixed:!markerView.markerFixed];
    }
}

- (void)updateWithMarkersMap:(AMMarkersMap *)_markersMap centerMarkerId:(NSInteger)_centerMarkerId {
    markersMap = _markersMap;

    for (AMMarkerView *markerView in [_markersViewsDict allValues]) {
        markerView.hidden = YES;
    }

    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    if (markersMap) {
        for (AMMarker *marker in markersMap.allValues) {
            AMMarkerView *markerView = _markersViewsDict[@(marker.id)];

            markerView.hidden = NO;

            CGPoint markerCenter = CGPointMake(self.bounds.size.width / 2.0f, self.bounds.size.height / 2.0f);

            markerCenter.x += marker.center.x * kMarkerViewSize;
            markerCenter.y += marker.center.y * kMarkerViewSize;

            markerView.center = markerCenter;

            markerView.transform = CGAffineTransformMakeRotation(marker.rotation);
        }
    }
}


@end
