//
//  Created by Michał Mizera on 19.04.2013.
//


#import "AMMarkerView.h"
#import "AMArEngine.h"

@implementation AMMarkerView
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupMarkerButton];
        self.markerFixed = NO;
    }

    return self;
}

- (void)setupMarkerButton {
    self.markerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.markerButton.alpha = 0.5f;
    [self.markerButton setBackgroundImage:[UIImage imageNamed:@"marker"] forState:UIControlStateNormal];
    self.markerButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    [self.markerButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [self addSubview:self.markerButton];
}

- (void)setMarkerAsFixed:(BOOL)fixed {
    self.markerFixed = fixed;

    [UIView animateWithDuration:0.3f animations:^{
        _markerButton.alpha = self.markerFixed ? 1.0f : 0.5f;
    }];
}

- (void)setMarkerId:(NSInteger)number {
    [self.markerButton setTitle:[NSString stringWithFormat:@"%d", number] forState:UIControlStateNormal];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    self.markerButton.frame = self.bounds;
}


@end
