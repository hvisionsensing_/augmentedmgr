//
//  Created by Michał Mizera on 19.04.2013.
//


#import <Foundation/Foundation.h>


@interface AMMarkerView : UIView
@property(nonatomic, strong) UIButton *markerButton;

@property(nonatomic) BOOL markerFixed;

- (void)setMarkerAsFixed:(BOOL)fixed;

- (void)setMarkerId:(NSInteger)number;
@end
