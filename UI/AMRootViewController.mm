#import "UIImage+OpenCV.h"
#import "AMRootViewController.h"

#import "AMRootView.h"
#import "AMMarker.h"
#import "AMMarkersMapView.h"
#import "Objection.h"
#import "AMMapGenerationViewController.h"
#import "AMCalibrationViewController.h"
#import "AMMapPreviewViewController.h"
#import "AMUserDefaultsManager.h"
#import "TMFConnector.h"
#import "AMUploadMapCommand.h"
#import "AMUploadMapCommandArguments.h"
#import "TMFKeyValueCommand.h"
#import "AMMarkersMap.h"
#import "AMDownloadMapCommand.h"
#import "AMGameplayViewController.h"
#import "AMDownloadGameStateCommand.h"

@interface AMRootViewController ()
@property(nonatomic, strong) AMArEngine *arEngine;
@property(nonatomic, strong) AMUserDefaultsManager *userDefaultsManager;
@end

@implementation AMRootViewController {
    TMFConnector *_connector;
    TMFPeer *_server;
    AMGameplayViewController *_gameplayViewController;
}

objection_requires_sel(@selector(arEngine), @selector(userDefaultsManager))

- (id)init {
    self = [super init];
    if (self) {
        _connector = [TMFConnector new];
        _connector.delegate = self;

        [_connector startDiscoveryWithCapabilities:@[[AMUploadMapCommand class], [AMDownloadGameStateCommand class]]
                                          delegate:self];
    }

    return self;
}

- (AMRootView *)castView {
    return (AMRootView *) self.view;
}

- (void)loadView {
    self.view = [[AMRootView alloc] initWithFrame:CGRectZero];

    [self.castView.createMapButton addTarget:self action:@selector(createMapButtonTapped)
                            forControlEvents:UIControlEventTouchUpInside];
    [self.castView.calibrateButton addTarget:self action:@selector(calibrateButtonTapped)
                            forControlEvents:UIControlEventTouchUpInside];
    [self.castView.previewMapButton addTarget:self action:@selector(previewMapButtonTapped)
                             forControlEvents:UIControlEventTouchUpInside];
    [self.castView.gameplayButton addTarget:self action:@selector(gameplayButtonTapped)
                           forControlEvents:UIControlEventTouchUpInside];

    [self.castView.saveMapButton addTarget:self action:@selector(saveMapButtonTapped)
                          forControlEvents:UIControlEventTouchUpInside];
    [self.castView.downloadMapButton addTarget:self action:@selector(downloadMapButtonTapped)
                              forControlEvents:UIControlEventTouchUpInside];
    [self.castView.uploadMapButton addTarget:self action:@selector(uploadMapButtonTapped)
                            forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self refreshMarkerMapSizeLabel];
    [_arEngine addObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [_arEngine removeObserver:self];
}

- (void)nextFrameReady {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImage *image = [UIImage imageWithCVMat:*(_arEngine.image)];
        self.castView.cameraImage.image = image;
    });
}

#pragma mark -
#pragma mark Button actions


- (void)gameplayButtonTapped {
    _gameplayViewController = [[JSObjection defaultInjector] getObject:[AMGameplayViewController class]
                                                                                   argumentList:@[_connector, _server]];
    _gameplayViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;

    [self presentViewController:_gameplayViewController animated:YES completion:nil];
}

- (void)previewMapButtonTapped {
    AMMapPreviewViewController *previewMapViewController = [JSObjection defaultInjector][[AMMapPreviewViewController class]];
    previewMapViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;

    [self presentViewController:previewMapViewController animated:YES completion:nil];
}

- (void)createMapButtonTapped {
    AMMapGenerationViewController *mapGenerationViewController = [JSObjection defaultInjector][[AMMapGenerationViewController class]];
    mapGenerationViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;

    [self presentViewController:mapGenerationViewController animated:YES completion:nil];
}

- (void)calibrateButtonTapped {
    AMCalibrationViewController *calibrationViewController = [JSObjection defaultInjector][[AMCalibrationViewController class]];
    calibrationViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;

    [self presentViewController:calibrationViewController animated:YES completion:nil];
}


- (void)uploadMapButtonTapped {
    if (_server) {
        AMUploadMapCommandArguments *args = [[AMUploadMapCommandArguments alloc] init];
        args.serializedMap = _userDefaultsManager.markersMap.serializedMap;

        [_connector sendCommand:[AMUploadMapCommand class] arguments:args
                    destination:_server response:^(id response, TMFPeer *peer, NSError *error) {

            NSLog(@"Wysłano mape");

            if (error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload map error"
                                                                    message:error.localizedDescription
                                                                   delegate:nil cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                });
            }
        }];
    }
}

- (void)downloadMapButtonTapped {
    if (_server) {
        [_connector sendCommand:[AMDownloadMapCommand class] arguments:nil
                    destination:_server response:^(NSString *serializedMap, TMFPeer *peer, NSError *error) {
            if (!error) {
                [_userDefaultsManager.markersMap loadMap:serializedMap];

                NSLog(@"Pobrano mape");

                if (error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download map error"
                                                                        message:error.localizedDescription
                                                                       delegate:nil cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    });
                }
            }

            [self refreshMarkerMapSizeLabel];
        }];
    }


}

- (void)saveMapButtonTapped {
    [_userDefaultsManager saveMarkerMap];
}


- (void)refreshMarkerMapSizeLabel {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.castView setMarkersSize:_userDefaultsManager.markersMap.count];
    });
}

#pragma mark -
#pragma mark TMFConnectorDelegate


- (void)connector:(TMFConnector *)connector didChangeDiscoveringPeer:(TMFPeer *)peer forChangeType:(TMFPeerChangeType)changeType {
    switch (changeType) {
        case TMFPeerChangeFound:
        case TMFPeerChangeUpdate:
            _server = peer;

            NSLog(@"New server found");
            [[self castView] setNetworkButtonActive:YES];

            break;

        case TMFPeerChangeRemove:
            NSLog(@"Server removed");
            [[self castView] setNetworkButtonActive:NO];
            _server = nil;
            break;
    }
}

- (void)connector:(TMFConnector *)connector didAddSubscription:(TMFPeer *)peer forCommand:(Class)commandClass {
    [_gameplayViewController didAddSubscription];
}

@end
