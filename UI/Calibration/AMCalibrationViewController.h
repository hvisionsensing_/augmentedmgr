//
//  Created by Michał Mizera on 08.10.2013.
//


#import <Foundation/Foundation.h>
#import "AMArEngineDelegate.h"
#import "AMCalibrator.h"


@interface AMCalibrationViewController : UIViewController <AMArEngineDelegate, AMCalibratorDelegate>
@end
