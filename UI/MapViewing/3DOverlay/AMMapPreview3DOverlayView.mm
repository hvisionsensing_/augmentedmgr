//
//  Created by Michał Mizera on 13.10.2013.
//


#import <Objection/Objection.h>
#import "AMMapPreview3DOverlayView.h"
#import "AMMarker.h"
#import "AMConverter.h"
#import "AMArEngine.h"
#import "NSLayoutConstraint+PLVisualAttributeConstraints.h"
#import "AMMarkersMap.h"
#import "AMMarkersDetector.h"
#import "AMCameraPose.h"
#import "AMArEngineHelpers.h"
#import "AMUserDefaultsManager.h"

@interface AMMapPreview3DOverlayView ()
@property(nonatomic, strong) AMArEngine *arEngine;
@property(nonatomic, strong) AMUserDefaultsManager *userDefaultsManager;
@end

@implementation AMMapPreview3DOverlayView {
    NGLCamera *_camera;
    NSMutableArray *_meshes;
    UILabel *_debugLabel;
}

objection_requires_sel(@selector(arEngine), @selector(userDefaultsManager))

- (void)awakeFromObjection {
    [super awakeFromObjection];

    self.backgroundColor = [UIColor clearColor];
    self.delegate = self;

    _camera = [[NGLCamera alloc] initWithMeshes:nil];


    float fov = (_arEngine.fieldOfView.width + _arEngine.fieldOfView.height) / 2.0f;
    fov = fov > 0 ? fov : 39;
    NSLog(@"fov = %f", fov);
    CGFloat aspect = _arEngine.imageSize.height > 0 ? _arEngine.imageSize.width / _arEngine.imageSize.height : 480.0f / 640.0f;
    NSLog(@"aspect = %f", aspect);
    [_camera lensPerspective:aspect near:0.001 far:10000 angle:fov];


    [NGLLight defaultLight].y = 10;
    [NGLLight defaultLight].z = 30;
    [NGLLight defaultLight].attenuation = 100;

    // Starts the debug monitor.
    [[NGLDebug debugMonitor] startWithView:self];

    _debugLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _debugLabel.font = [UIFont systemFontOfSize:12];
    _debugLabel.numberOfLines = 0;
    _debugLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_debugLabel];

    NSDictionary *views = @{
            @"self" : self,
            @"debugLabel" : _debugLabel
    };

    [self addConstraints:[NSLayoutConstraint attributeConstraintsWithVisualFormatsArray:@[
            @"debugLabel.right == self.right",
            @"debugLabel.top == self.top + 50"

    ]
                                                                                  views:views]];
}

- (void)setupWithMarkersMap:(AMMarkersMap *)markersMap {
    [_camera removeAllMeshes];

    NSArray *markers = [markersMap allValues];
    CGFloat realMarkerSize = _userDefaultsManager.markerSize;

    for (AMMarker *marker in markers) {


        NSString *cubeSize = [NSString stringWithFormat:@"%f", realMarkerSize];
        NSString *arrowSize = [NSString stringWithFormat:@"%f", realMarkerSize / 2.0f];
        NGLMesh *mesh = [[NGLMesh alloc] initWithFile:@"cube.obj"
                                             settings:@{kNGLMeshKeyNormalize : cubeSize, kNGLMeshKeyCentralize : kNGLMeshCentralizeYes}
                                             delegate:nil];

        NGLMesh *arrowMesh = [[NGLMesh alloc] initWithFile:@"arrowPointer.obj"
                                                  settings:@{kNGLMeshKeyNormalize : arrowSize}
                                                  delegate:nil];

        [arrowMesh translateToX:marker.center.x * realMarkerSize toY:-marker.center.y * realMarkerSize toZ:12.5];

        CGFloat rotation = atan2(marker.center.x, marker.center.y);
        CGFloat rot2 = nglRadiansToDegrees(rotation) + 180.0f;
        arrowMesh.rotateZ = rot2;

        [mesh translateToX:marker.center.x * realMarkerSize toY:-marker.center.y * realMarkerSize toZ:0.5];


        mesh.rotateZ = -nglRadiansToDegrees(marker.rotation);

        if (marker.center != cv::Point2f(0, 0)) {
            [_camera addMesh:arrowMesh];
        }
        [_camera addMesh:mesh];
    }
}

- (void)updateCameraWithPose:(AMCameraPose *)cameraPose {
    @synchronized (self) {
        NGLmat4 matrix;
        [AMConverter nglMatFromOpenCVMat:cameraPose.matrix nglMat:&matrix];
        [_camera rebaseWithMatrix:matrix scale:1 compatibility:NGLRebaseQualcommAR];
    }
}

- (void)drawView {
    @synchronized (self) {
        [super drawView];

        [_camera drawCamera];
    }
}

@end
