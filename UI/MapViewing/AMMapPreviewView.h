//
//  Created by Michał Mizera on 02.11.2013.
//


#import <Foundation/Foundation.h>

@class AMMapPreview3DOverlayView;
@class AMMarkersMap;


@interface AMMapPreviewView : UIView
@property(nonatomic, strong) UIImageView *cameraImage;
@property(nonatomic, strong) UIButton *backButton;
@property(nonatomic, strong) AMMapPreview3DOverlayView *overlay3d;
@property(nonatomic, strong) UILabel *noMapFoundLabel;

- (id)initWithFrame:(CGRect)frame andMarkersMap:(AMMarkersMap *)markersMap;

- (void)setDistance:(CGFloat)distance1;
@end
