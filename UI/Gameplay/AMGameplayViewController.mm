//
//  Created by Michał Mizera on 16.11.2013.
//


#import <Objection/Objection.h>
#import <NinevehGL/NinevehGL.h>
#import "AMGameplayViewController.h"
#import "AMArEngine.h"
#import "AMUserDefaultsManager.h"
#import "AMGameplayView.h"
#import "UIImage+OpenCV.h"
#import "AMGameplay3DOverlayView.h"
#import "AMDownloadGameStateCommand.h"
#import "AMMarkersMap.h"
#import "AMDefines.h"
#import "AMStartGameCommand.h"
#import "AMStartGameCommandArguments.h"
#import "AMCameraPose.h"
#import "AMMarkersDetector.h"
#import "AMMovementStickView.h"
#import "AMMoveCarCommandArguments.h"
#import "AMMoveCarCommand.h"
#import "AMConverter.h"


@interface AMGameplayViewController ()
@property(nonatomic, strong) AMArEngine *arEngine;
@property(nonatomic, strong) AMUserDefaultsManager *userDefaultsManager;
@end

@implementation AMGameplayViewController {
    TMFConnector *_connector;
    TMFPeer *_server;
    BOOL _startCommandSent;
}

objection_initializer_sel(@selector(initConnector:server:))
objection_requires_sel(@selector(arEngine), @selector(userDefaultsManager))

- (id)initConnector:(TMFConnector *)connector server:(TMFPeer *)server {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _connector = connector;
        _server = server;
        _startCommandSent = NO;

        [self subscribeToGameStateUpdates];
    }

    return self;
}

- (void)subscribeToGameStateUpdates {
    __block NSArray *gameStateArray;
    [_connector subscribe:[AMDownloadGameStateCommand class] peer:_server
                  receive:^(TMFKeyValueCommandArguments *arguments, TMFPeer *peer) {
                      gameStateArray = [NSJSONSerialization JSONObjectWithData:[arguments.value dataUsingEncoding:NSUTF8StringEncoding]
                                                                       options:nil
                                                                         error:nil];

                      [self.castView.overlay3d updateWithGameState:gameStateArray];
                  }
               completion:^(NSError *error) {
                   if (error) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Subscribe error"
                                                                           message:error.localizedDescription
                                                                          delegate:nil cancelButtonTitle:@"OK"
                                                                 otherButtonTitles:nil];
                           [alert show];
                       });
                   }
               }];
}

- (AMGameplayView *)castView {
    return (AMGameplayView *) self.view;
}

- (void)loadView {
    self.view = [[AMGameplayView alloc] initWithFrame:CGRectZero andMarkersMap:_userDefaultsManager.markersMap];

    [self.castView.backButton addTarget:self action:@selector(backButtonTapped)
                       forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.castView.noMapFoundLabel.hidden = _userDefaultsManager.markersMap != nil;


    // Don't show control center in iOS7
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.castView layoutIfNeeded];
    [self.castView.overlay3d setupMovementStick];
    [self update];
    _arEngine.mode = AMArEngineModeGameplay;
    [_arEngine addObserver:self];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    _arEngine.mode = AMArEngineModeDisabled;
    [_arEngine removeObserver:self];
}


- (void)nextFrameReady {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.castView.cameraImage.image = [UIImage imageWithCVMat:*(_arEngine.image)];
        [self.castView setDistance:[AMConverter calculateDistance:_arEngine.cameraPose]];
    });

    if (_arEngine.cameraPose && _arEngine.markers.count > 0) {
        [self.castView.overlay3d updateCameraWithPose:_arEngine.cameraPose];

        if (!_startCommandSent) {
            [self sendStartCommand];
        }
    }
}

- (void)sendStartCommand {
    _startCommandSent = YES;
    AMStartGameCommandArguments *gameStartArguments = [[AMStartGameCommandArguments alloc] init];
    gameStartArguments.x = _arEngine.cameraPose.tvecInv.at<double>(0, 0) / _userDefaultsManager.markerSize;
    gameStartArguments.y = -_arEngine.cameraPose.tvecInv.at<double>(1, 0) / _userDefaultsManager.markerSize;
    [_connector sendCommand:[AMStartGameCommand class] arguments:gameStartArguments destination:_server
                   response:^(id response, TMFPeer *peer, NSError *error) {
                   }];
}

#pragma mark -
#pragma mark Button actions

- (void)backButtonTapped {
    [_connector unsubscribe:[AMDownloadGameStateCommand class] fromPeer:_server completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark Moving car


- (void)update {
    @synchronized (self) {
        CGPoint stickPosition = self.castView.overlay3d.movementStick.movement;
        if (stickPosition.x != 0.0 || stickPosition.y != 0.0) {
            AMMoveCarCommandArguments *moveCarArguments = [[AMMoveCarCommandArguments alloc] init];
            moveCarArguments.x = -stickPosition.x;
            moveCarArguments.y = stickPosition.y;
            [_connector sendCommand:[AMMoveCarCommand class] arguments:moveCarArguments destination:_server
                           response:^(id response, TMFPeer *peer, NSError *error) {

                           }];
        }

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (1.0 / 30.0 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self update];
        });
    }
}

#pragma mark -
#pragma mark TMFConnectorDelegate

- (void)didAddSubscription {
    self.castView.onlineText.hidden = NO;
}

- (void)didRemoveSubscription {
    self.castView.onlineText.hidden = YES;
    [self subscribeToGameStateUpdates];
}
@end
