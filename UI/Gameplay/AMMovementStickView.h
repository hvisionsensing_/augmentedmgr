#import <UIKit/UIKit.h>
#import <NinevehGL/NinevehGL.h>

static inline float distanceBetweenPoints(CGPoint pointA, CGPoint pointB) {
    float xDistace = (float) fabs(pointA.x - pointB.x);
    float yDistance = (float) fabs(pointA.y - pointB.y);

    return (float) sqrt(xDistace * xDistace + yDistance * yDistance);
}


@interface AMMovementStickView : UIView

@property(nonatomic) CGPoint innerPosition;
@property(nonatomic) CGPoint outerPosition;
@property(nonatomic, weak) UITouch *touch;
@property(nonatomic, readonly) CGPoint movement;

@end
