//
//  Created by Michał Mizera on 16.11.2013.
//


#import <Foundation/Foundation.h>
#import <NinevehGL/NinevehGL.h>

@class AMCameraPose;
@class AMMovementStickView;


@interface AMGameplay3DOverlayView : NGLView

- (void) updateCameraWithPose:(AMCameraPose *)cameraPose;

- (void)updateWithGameState:(NSArray *)dictionary;

- (void)setupMovementStick;

@property(nonatomic, strong) AMMovementStickView *movementStick;
@end
