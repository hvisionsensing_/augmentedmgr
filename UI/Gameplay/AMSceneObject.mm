//
//  Created by Michał Mizera on 24.11.2013.
//


#import <NinevehGL/NinevehGL.h>
#import "AMSceneObject.h"
#import "AMDefines.h"

CGFloat AMMeshObjectSinglePhysicStep = 1.0f / 10.0f;

@implementation AMSceneObject {

@private
    CGFloat _progress;

    CGFloat _targetAngle;
    CGFloat _targetY;
    CGFloat _targetX;
    CGFloat _sourceAngle;
    CGFloat _sourceY;
    CGFloat _sourceX;
}

+ (instancetype)objectWithMesh:(NGLMesh *)mesh x:(CGFloat)x y:(CGFloat)y angle:(CGFloat)angle {
    return [[self alloc] initWithMesh:mesh x:x y:y angle:angle];
}

- (instancetype)initWithMesh:(NGLMesh *)mesh x:(CGFloat)x y:(CGFloat)y angle:(CGFloat)angle {
    self = [super init];
    if (self) {
        _targetAngle = _sourceAngle = angle;
        _targetX = _sourceX = x;
        _targetY = _sourceY = y;
        _progress = 1.0f;

        _mesh = mesh;
        [self moveImmediatelyToX:x y:y angle:angle];
    }

    return self;
}

- (void)moveImmediatelyToX:(CGFloat)x y:(CGFloat)y angle:(CGFloat)angle {
    _mesh.x = x;
    _mesh.y = y;
    _mesh.rotateZ = angle;
}

- (void)animateToX:(CGFloat)x y:(CGFloat)y angle:(CGFloat)angle {
    [self moveImmediatelyToX:_targetX y:_targetY angle:_targetAngle];

    _sourceX = _targetX;
    _sourceY = _targetY;
    _sourceAngle = _targetAngle;

    _targetX = x;
    _targetY = y;
    _targetAngle = angle;
    _progress = 0;
}

- (void)updateWithDt:(NSTimeInterval)dt {
    _progress = (CGFloat) MIN(1.0f, _progress + (dt / AMMeshObjectSinglePhysicStep));

    [self moveImmediatelyToX:lerp(_sourceX, _targetX, _progress)
                           y:lerp(_sourceY, _targetY, _progress)
                       angle:lerp(_sourceAngle, _targetAngle, _progress)];
}

@end
