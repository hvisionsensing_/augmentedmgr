//
//  Created by Michał Mizera on 24.11.2013.
//


#import <Foundation/Foundation.h>

@class NGLMesh;


@interface AMSceneObject : NSObject

@property NGLMesh *mesh;

+ (instancetype)objectWithMesh:(NGLMesh *)mesh x:(CGFloat)x y:(CGFloat)y angle:(CGFloat)angle;

- (void)animateToX:(CGFloat)x y:(CGFloat)y angle:(CGFloat)angle;

- (void)updateWithDt:(NSTimeInterval)dt;
@end
