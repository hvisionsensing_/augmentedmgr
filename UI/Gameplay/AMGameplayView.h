//
//  Created by Michał Mizera on 16.11.2013.
//


#import <Foundation/Foundation.h>

@class AMMarkersMap;
@class AMGameplay3DOverlayView;


@interface AMGameplayView : UIView
@property(nonatomic, strong) UIImageView *cameraImage;
@property(nonatomic, strong) UIButton *backButton;
@property(nonatomic, strong) AMGameplay3DOverlayView *overlay3d;
@property(nonatomic, strong) UILabel *noMapFoundLabel;

@property(nonatomic, strong) UILabel *onlineText;

@property(nonatomic, strong) UILabel *distanceLabel;

- (id)initWithFrame:(CGRect)frame andMarkersMap:(AMMarkersMap *)markersMap;

- (void)setDistance:(CGFloat)distance;
@end
