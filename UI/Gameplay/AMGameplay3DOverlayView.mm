//
//  Created by Michał Mizera on 16.11.2013.
//


#import <Objection/Objection.h>
#import <PLVisualAttributeConstraints/NSLayoutConstraint+PLVisualAttributeConstraints.h>
#import <NinevehGL/NinevehGL.h>
#import "AMGameplay3DOverlayView.h"
#import "AMArEngine.h"
#import "AMCameraPose.h"
#import "AMConverter.h"
#import "AMDownloadGameStateCommand.h"
#import "AMMarkersDetector.h"
#import "AMMovementStickView.h"
#import "AMMovementStickView.h"
#import "AMUserDefaultsManager.h"
#import "AMSceneObject.h"

NSString *AMPhysicObjectMeshNameIdentifier = @"mesh";
NSString *AMPhysicObjectSizeIdentifier = @"size";


@interface AMGameplay3DOverlayView ()
@property(nonatomic, strong) AMArEngine *arEngine;
@property(nonatomic, strong) AMUserDefaultsManager *userDefaultsManager;
@end

@implementation AMGameplay3DOverlayView {
    NGLCamera *_camera;
    NSMutableDictionary *_meshes;
    UILabel *_debugLabel;
    NSDictionary *_meshesLibrary;
    AMMovementStickView *_movementStick;
}

objection_requires_sel(@selector(arEngine), @selector(userDefaultsManager))

- (void)awakeFromObjection {
    [super awakeFromObjection];

    self.backgroundColor = [UIColor clearColor];
    self.delegate = self;

    self.userInteractionEnabled = YES;

    _meshes = [NSMutableDictionary dictionary];
    _camera = [[NGLCamera alloc] initWithMeshes:nil];

    float fov = (_arEngine.fieldOfView.width + _arEngine.fieldOfView.height) / 2.0f;
    fov = fov > 0 ? fov : 39;
    NSLog(@"fov = %f", fov);
    CGFloat aspect = _arEngine.imageSize.height > 0 ? _arEngine.imageSize.width / _arEngine.imageSize.height : 480.0f / 640.0f;
    NSLog(@"aspect = %f", aspect);
    [_camera lensPerspective:aspect near:0.001 far:10000 angle:fov];


    [NGLLight defaultLight].y = 10;
    [NGLLight defaultLight].z = 30;
    [NGLLight defaultLight].attenuation = 100;

    [[NGLDebug debugMonitor] startWithView:self];

    _debugLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _debugLabel.font = [UIFont systemFontOfSize:12];
    _debugLabel.numberOfLines = 0;
    _debugLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_debugLabel];

    NSDictionary *views = @{
            @"self" : self,
            @"debugLabel" : _debugLabel
    };

    [self addConstraints:[NSLayoutConstraint attributeConstraintsWithVisualFormatsArray:@[
            @"debugLabel.right == self.right",
            @"debugLabel.top == self.top + 50"

    ]
                                                                                  views:views]];

    [self setupMeshesLibrary];
}


- (void)setupMeshesLibrary {
    _meshesLibrary = @{
            @(MSPhysicObjectTypeTree) : @{
                    AMPhysicObjectMeshNameIdentifier : @"tree.dae",
                    AMPhysicObjectSizeIdentifier : @3.0f,
            },
            @(MSPhysicObjectTypeBox) : @{
                    AMPhysicObjectMeshNameIdentifier : @"WoodenBox02.obj",
                    AMPhysicObjectSizeIdentifier : @0.4f,
            },
            @(MSPhysicObjectTypeRock) : @{
                    AMPhysicObjectMeshNameIdentifier : @"rock.dae",
                    AMPhysicObjectSizeIdentifier : @0.6f,
            },
            @(MSPhysicObjectTypeCone) : @{
                    AMPhysicObjectMeshNameIdentifier : @"cone.dae",
                    AMPhysicObjectSizeIdentifier : @0.3f,
            },

            @(MSPhysicObjectTypeCar1) : @{
                    AMPhysicObjectMeshNameIdentifier : @"car1.dae",
                    AMPhysicObjectSizeIdentifier : @1.0f,
            },
            @(MSPhysicObjectTypeCar2) : @{
                    AMPhysicObjectMeshNameIdentifier : @"car2.dae",
                    AMPhysicObjectSizeIdentifier : @1.0f,
            },
            @(MSPhysicObjectTypeCar3) : @{
                    AMPhysicObjectMeshNameIdentifier : @"car3.dae",
                    AMPhysicObjectSizeIdentifier : @1.0f,
            },
            @(MSPhysicObjectTypeCar4) : @{
                    AMPhysicObjectMeshNameIdentifier : @"car4.dae",
                    AMPhysicObjectSizeIdentifier : @1.0f,
            },
    };
}


- (void)updateCameraWithPose:(AMCameraPose *)cameraPose {
    @synchronized (self) {
        NGLmat4 matrix;
        [AMConverter nglMatFromOpenCVMat:cameraPose.matrix nglMat:&matrix];
        [_camera rebaseWithMatrix:matrix scale:1 compatibility:NGLRebaseQualcommAR];
    }
}

- (void)drawView {
    @synchronized (self) {
        [super drawView];

        static NSDate *lastFrameDate = [NSDate date];
        NSTimeInterval dt = -[lastFrameDate timeIntervalSinceNow];
        lastFrameDate = [NSDate date];

        [self updateAllSceneObjects:dt];
        [_camera drawCamera];
    }
}

- (void)updateAllSceneObjects:(NSTimeInterval)dt {
    for (AMSceneObject *sceneObject in _meshes.allValues) {
        [sceneObject updateWithDt:dt];
    }
}

- (void)updateWithGameState:(NSArray *)gameStateArray {
    @synchronized (self) {
        NSMutableArray *currentIds = [NSMutableArray arrayWithCapacity:gameStateArray.count];

        for (NSDictionary *physicObjectDict in gameStateArray) {
            NSInteger id = [physicObjectDict[MSPhysicObjectIdIdentifier] intValue];
            MSPhysicObjectType type = (MSPhysicObjectType) [physicObjectDict[MSPhysicObjectTypeIdentifier] intValue];
            CGFloat x = [physicObjectDict[MSPhysicObjectXPosIdentifier] floatValue];
            CGFloat y = [physicObjectDict[MSPhysicObjectYPosIdentifier] floatValue];
            CGFloat angle = [physicObjectDict[MSPhysicObjectAngleIdentifier] floatValue];

            x *= _userDefaultsManager.markerSize;
            y *= _userDefaultsManager.markerSize;
            angle = nglRadiansToDegrees(angle);

            [currentIds addObject:@(id)];

            AMSceneObject *sceneObject = _meshes[@(id)];
            if (!sceneObject) {
                CGFloat size = [_meshesLibrary[@(type)][AMPhysicObjectSizeIdentifier] floatValue] * _userDefaultsManager.markerSize;
                NSString *meshPath = _meshesLibrary[@(type)][AMPhysicObjectMeshNameIdentifier];
                NGLMesh *newMesh = [[NGLMesh alloc] initWithFile:meshPath
                                                        settings:@{kNGLMeshKeyNormalize : @(size), kNGLMeshKeyCentralize : kNGLMeshCentralizeYes}
                                                        delegate:nil];
                [_camera addMesh:newMesh];

                sceneObject = [AMSceneObject objectWithMesh:newMesh x:x y:y angle:angle];
                _meshes[@(id)] = sceneObject;
            }
            else {
                [sceneObject animateToX:x y:y angle:angle];
            }


        }

        NSMutableArray *meshesIdsToDelete = [NSMutableArray array];

        for (NSNumber *meshId in _meshes.allKeys) {
            if (![currentIds containsObject:meshId]) {
                [meshesIdsToDelete addObject:meshId];
            }
        }

        for (NSNumber *meshId in meshesIdsToDelete) {
            AMSceneObject *sceneObject = _meshes[meshId];
            [_camera removeMesh:sceneObject.mesh];
        }
        [_meshes removeObjectsForKeys:meshesIdsToDelete];
    }
}

#pragma mark -
#pragma mark Touches


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];

    CGSize size = self.bounds.size;
    CGPoint leftCorner = CGPointMake(50, size.height - 50);
    float radius = size.width * 0.2f;

    for (UITouch *touch in touches) {
        CGPoint point = [touch locationInView:self];

        if (distanceBetweenPoints(point, leftCorner) <= radius) {
            _movementStick.outerPosition = point;
            _movementStick.touch = touch;
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];

    for (UITouch *touch in touches) {
        CGPoint point = [touch locationInView:self];

        if (_movementStick.touch == touch) {
            _movementStick.innerPosition = point;
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];

    for (UITouch *touch in touches) {
        if (_movementStick.touch == touch) {
            _movementStick.touch = nil;
        }
    }
}

- (void)setupMovementStick {
    _movementStick = [[AMMovementStickView alloc] init];
    _movementStick.outerPosition = CGPointMake(100, self.frame.size.height - 100);
    [self addSubview:_movementStick];
}

@end
