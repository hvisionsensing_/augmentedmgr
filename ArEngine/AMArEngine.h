#import <OpenCV/opencv2/highgui/cap_ios.h>

@protocol AMArEngineDelegate;
@class AMMarker;
@class AMCameraPose;

typedef NS_ENUM(NSInteger, AMArEngineMode) {
    AMArEngineModeCalibrating,
    AMArEngineModeMapGeneration,
    AMArEngineModeGameplay,
    AMArEngineModeDisabled,
};

@interface AMArEngine : NSObject <CvVideoCameraDelegate>
@property(nonatomic) cv::Mat *image;

@property(nonatomic) AMArEngineMode mode;

@property(nonatomic) CGSize imageSize;

@property(nonatomic) CGSize fieldOfView;

- (void)addObserver:(id <AMArEngineDelegate>)observer;

- (void)removeObserver:(id <AMArEngineDelegate>)observer;

- (cv::Mat)currentCameraMatrix;

- (NSArray *)markers;

- (NSDictionary *)markersDict;

- (cv::Mat *)undistortedImage;

- (AMCameraPose *)cameraPose;
@end

