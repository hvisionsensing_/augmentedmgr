//
//  Created by Michał Mizera on 17.04.2013.
//


#import <Foundation/Foundation.h>

@protocol AMArEngineDelegate <NSObject>

- (void)nextFrameReady;

@end
