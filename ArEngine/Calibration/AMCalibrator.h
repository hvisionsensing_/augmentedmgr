//
//  Created by Michał Mizera on 05.10.2013.
//


#import <Foundation/Foundation.h>


using namespace std;
using namespace cv;

@protocol AMCalibratorDelegate <NSObject>
- (void)updateProgressBarWithValue:(CGFloat)progress;
@end

@interface AMCalibrator : NSObject

@property(nonatomic) std::vector<cv::Point2f> corners;
@property(nonatomic) id<AMCalibratorDelegate> delegate;

@property(nonatomic) NSUInteger lastPresentedProcessedImage;

@property(nonatomic) vector<vector<Point2f> > *approvedCorners;

- (void)startCalibration;

- (void)endCalibration;

- (void)saveRawImage:(UIImage *)image;

- (void)processSavedImages;

- (UIImage *)loadNextProcessedImage;

- (void)acceptCurrentProcessedImage;
@end
