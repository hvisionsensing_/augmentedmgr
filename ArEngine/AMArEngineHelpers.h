//
//  Created by Michał Mizera on 01.11.2013.
//


#import <Foundation/Foundation.h>
#import "core.hpp"

@class AMMarker;

using namespace cv;

@interface AMArEngineHelpers : NSObject
+ (float)distanceWithPoint:(Point2f)p1 point2:(Point2f)p2;

+ (Point2f)rotatePoint:(Point2f)point withPivot:(Point2f)pivot byAngle:(float)theta;

+ (int)checkParity:(short)digits;

+ (int)hammingCheck:(short)digits;

+ (int)getMarkerID:(Mat)markerMat markerOut:(AMMarker **)markerOut;

+ (float)computeRotation:(Point2f)top center:(Point2f)center size:(float)size;
@end
