//
//  Created by Michał Mizera on 01.11.2013.
//


#import "AMArEngineHelpers.h"

#import "AMMarker.h"

using namespace cv;

@implementation AMArEngineHelpers

+ (float)distanceWithPoint:(cv::Point2f)p1 point2:(cv::Point2f)p2 {
    return (float) sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

+ (cv::Point2f)rotatePoint:(cv::Point2f)point withPivot:(cv::Point2f)pivot byAngle:(float)theta {
    cv::Point2f returnPoint = cv::Point2f();

    returnPoint.x = (cos(theta) * (point.x - pivot.x) - sin(theta) * (point.y - pivot.y) + pivot.x);
    returnPoint.y = (sin(theta) * (point.x - pivot.x) + cos(theta) * (point.y - pivot.y) + pivot.y);

    return returnPoint;
}

+ (int)checkParity:(short)digits {
    int output = 1;
    for (int i = 0; i < 9; i++)
        output ^= (digits >> i) & 1;
    return output;
}

+ (int)hammingCheck:(short)digits {
    return [self checkParity:digits & 341] &&
            [self checkParity:digits & 102] &&
            [self checkParity:digits & 120] &&
            [self checkParity:digits & 384];
}

+ (int)getMarkerID:(cv::Mat)markerMat markerOut:(AMMarker **)markerOut {
    int blockSize = markerMat.rows / 7;
    int blockSizeRoot = blockSize * blockSize;
    cv::Mat square;

    // detecting contour (outside ring)
    for (int x = 0; x < 7; x++)
        for (int y = 0; y < 7; y += (x == 0 || x == 6) ? 1 : 6) {
            square = markerMat(cv::Rect(x * blockSize, y * blockSize, blockSize, blockSize));
            if (countNonZero(square) > (blockSizeRoot) * 0.75)
                return -1;
        }

    // there is outside ring
    (*markerOut).rotation = -1;

    // detecting "top" block
    for (int x = 1; x < 6; x++)
        for (int y = 1; y < 6; y += (x == 1 || x == 5) ? 1 : 4) {

            square = markerMat(cv::Rect(x * blockSize, y * blockSize, blockSize, blockSize));
            if (countNonZero(square) < (blockSizeRoot) >> 1) {
                if ((*markerOut).rotation >= 0) // too much rotation block found
                {
                    return -2;
                }

                if (x == 3 && y == 1) // top
                {
                    (*markerOut).rotation = 0;
                }
                else if (x == 5 && y == 3) // right
                {
                    (*markerOut).rotation = (float) M_PI_2;
                }
                else if (x == 3 && y == 5) // bottom
                {
                    (*markerOut).rotation = (float) M_PI;
                }
                else if (x == 1 && y == 3) // left
                {
                    (*markerOut).rotation = (float) (M_PI_2 * 3.0f);
                }
                else {
                    // wrong rotation block
                    return -3;
                }
            }
        }

    // no rotation block found
    if ((*markerOut).rotation < 0)
        return -4;

    short id = 0;
    short idIterator = 256;//binary 100000000
    float epsilon = 0.1f;

    if ((*markerOut).rotation < epsilon) //top
    {
        for (int y = 2; y < 5; y++)
            for (int x = 2; x < 5; x++) {
                square = markerMat(cv::Rect(x * blockSize, y * blockSize, blockSize, blockSize));
                if (countNonZero(square) < (blockSize * blockSize) >> 1)
                    id = id | idIterator;
                idIterator >>= 1;
            }
    }
    else if (fabs((*markerOut).rotation - M_PI_2) < epsilon) //right
    {
        for (int x = 4; x >= 2; x--)
            for (int y = 2; y < 5; y++) {
                square = markerMat(cv::Rect(x * blockSize, y * blockSize, blockSize, blockSize));
                if (countNonZero(square) < (blockSize * blockSize) >> 1)
                    id = id | idIterator;
                idIterator >>= 1;
            }
    }
    else if (fabs((*markerOut).rotation - M_PI) < epsilon) //bottom
    {
        for (int y = 4; y >= 2; y--)
            for (int x = 4; x >= 2; x--) {
                square = markerMat(cv::Rect(x * blockSize, y * blockSize, blockSize, blockSize));
                if (countNonZero(square) < (blockSize * blockSize) >> 1)
                    id = id | idIterator;
                idIterator >>= 1;
            }
    }
    else if (fabs((*markerOut).rotation - M_PI_2 * 3.0f) < epsilon) //left
    {
        for (int x = 2; x < 5; x++)
            for (int y = 4; y >= 2; y--) {
                square = markerMat(cv::Rect(x * blockSize, y * blockSize, blockSize, blockSize));
                if (countNonZero(square) < (blockSize * blockSize) >> 1)
                    id = id | idIterator;
                idIterator >>= 1;
            }
    }

    if (![self hammingCheck:id])
        return -5;

    (*markerOut).id = (id >> 4 & 16) + (id >> 3 & 14) + (id >> 2 & 1);

    if ((*markerOut).id == 0)
        return -6;
    /// we're not using marker with id=0, because it's white marker

    return 0;
}


+ (float)computeRotation:(cv::Point2f)top center:(cv::Point2f)center size:(float)size {
    // returns rotation from OY (range <-PI,PI>)
    cv::Point2f tmp = top - center;
    tmp *= 1.0 / size;
    float ret = static_cast<float>(acos(tmp.y / sqrt(tmp.x * tmp.x + tmp.y * tmp.y)));

    return ret;
}

@end
