//
//  Created by Michał Mizera on 03.11.2013.
//


#import <Objection/Objection.h>
#import "AMMarkersDetector.h"
#import "AMMarker.h"
#import "AMUserDefaultsManager.h"
#import "AMArEngineHelpers.h"
#import "AMArEngine.h"
#import "AMDefines.h"

using namespace std;
using namespace cv;

NSInteger kMinimalContourSize = 60;
NSTimeInterval kDisposeUnseenMarkerAfter = 0.2;

NSInteger kCanonicalMarkerSize = 200;

@interface AMMarkersDetector ()
@property(nonatomic) AMUserDefaultsManager *userDefaultsManager;
@property(nonatomic) AMArEngine *arEngine;
@end


@implementation AMMarkersDetector {
    std::vector <cv::Point2f> _markerPoints2D;
@private
    Mat *_canonicalMarker;
    Mat *_grayImage;
    Mat *_thresh;
}

objection_initializer_sel(@selector(initWithImageSize:))
objection_requires_sel(@selector(userDefaultsManager), @selector(arEngine))

- (id)initWithImageSize:(NSValue *)sizeObject {
    self = [super init];
    if (self) {
        CGSize size = [sizeObject CGSizeValue];
        _thresh = new cv::Mat((int) size.height, (int) size.width, CV_8UC1);
        _undistortedImage = new cv::Mat((int) size.width, (int) size.height, CV_8UC3);
        _grayImage = new cv::Mat((int) size.height, (int) size.width, CV_8UC1);
        _canonicalMarker = new cv::Mat(kCanonicalMarkerSize, kCanonicalMarkerSize, CV_8UC1);
        _markers = [NSMutableArray array];


        _markerPoints2D.push_back(cv::Point2f(0, 0));
        _markerPoints2D.push_back(cv::Point2f(kCanonicalMarkerSize - 1, 0));
        _markerPoints2D.push_back(cv::Point2f(kCanonicalMarkerSize - 1, kCanonicalMarkerSize - 1));
        _markerPoints2D.push_back(cv::Point2f(0, kCanonicalMarkerSize - 1));
    }

    return self;
}

- (void)currentMarkersInImage:(Mat *)image {
    cv::cvtColor(*image, *_undistortedImage, CV_BGR2GRAY);
    [self updateMarkersInImage:_undistortedImage drawBorders:YES];
}

- (void)markersInImage:(Mat *)image {
    std::vector<std::vector<cv::Point2i>> contours;
    AMMarker *marker;

    cv::cvtColor(*image, *_grayImage, CV_BGR2GRAY);
    cv::adaptiveThreshold(*_grayImage, *_thresh, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY_INV, 15, 5);

    cv::findContours(*_thresh, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    int size = contours.size();
    for (int i = 0; i < size; i++) {
        marker = [self getMarkerForContour:contours[i]
                                 grayImage:*_grayImage
                                 realImage:image
                              correctColor:cv::Scalar(0, 255, 0)
                                errorColor:cv::Scalar(0, 0, 255)
                                drawBorder:YES];

        if (marker) { // finding any valid marker will be enough to unwrap
            [self unwrapImageForMarkerWithCorners:marker.imagePoints
                                       inputImage:_grayImage
                                      outputImage:_arEngine.undistortedImage];

            if (_arEngine.undistortedImage->cols < 2000 && _arEngine.undistortedImage->rows < 2000) {
                [self updateMarkersInImage:_arEngine.undistortedImage drawBorders:YES];
            }
            break;
        }
    }
}

- (void)updateMarkersInImage:(Mat *)inputGrayImage drawBorders:(BOOL)drawBorders {
    NSMutableArray *currentMarkers = [NSMutableArray array];
    adaptiveThreshold(*inputGrayImage, *_thresh, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, 15, 5);
    *_grayImage = inputGrayImage->clone();
    cvtColor(*inputGrayImage, *inputGrayImage, CV_GRAY2RGB);

    // do rest
    std::vector<std::vector<cv::Point2i>> contours;
    findContours(*_thresh, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    int size = contours.size();

    for (int i = 0; i < size; i++) {
        AMMarker *marker = [self getMarkerForContour:contours[i]
                                           grayImage:*_grayImage
                                           realImage:inputGrayImage
                                        correctColor:Scalar(0, 255, 0)
                                          errorColor:Scalar(255, 0, 0)
                                          drawBorder:drawBorders];

        if (marker) {
            [currentMarkers addObject:marker];
        }
    }


    // matching found markers to existing ones
    for (AMMarker *currentMarker in currentMarkers) {
        bool found = false;
        for (AMMarker *marker in _markers) {
            /// marker found
            if (marker.id == currentMarker.id) {
                found = YES;
                [self updateMarker:marker withNewMarker:currentMarker];
                break;
            }
        }

        /// new marker
        if (!found) {
            [_markers addObject:currentMarker];
        }
    }

    // removing old markers
    for (NSInteger i = [_markers count] - 1; i >= 0; i--) {
        AMMarker *marker = _markers[(NSUInteger) i];
        if ([NSDate timeIntervalSinceReferenceDate] - marker.lastVisible > kDisposeUnseenMarkerAfter) {
            [_markers removeObjectAtIndex:(NSUInteger) i];
        }
    }
}


- (void)updateMarker:(AMMarker *)marker withNewMarker:(AMMarker *)newMarker {
    marker.center = newMarker.center;
    marker.size = newMarker.size;
    marker.rotation = newMarker.rotation;
    marker.lastVisible = [NSDate timeIntervalSinceReferenceDate];
    marker.imagePoints = newMarker.imagePoints;
}

- (AMMarker *)getMarkerForContour:(vector<Point2i>)contour grayImage:(Mat)grayImage realImage:(Mat *)realImage correctColor:(cv::Scalar)correctColor errorColor:(cv::Scalar)errorColor drawBorder:(BOOL)drawBorder {
    AMMarker *newMarker = nil;

    if (contour.size() > kMinimalContourSize) {
        std::vector<cv::Point2i> markerCornersInteger;
        std::vector<cv::Point2f> markerCornersFloat;
        approxPolyDP(contour, markerCornersInteger, float(contour.size()) * 0.1f, true);
        if (markerCornersInteger.size() == 4 && isContourConvex(markerCornersInteger)) {
            // sort
            int dx1 = markerCornersInteger[1].x - markerCornersInteger[0].x;
            int dy1 = markerCornersInteger[1].y - markerCornersInteger[0].y;
            int dx2 = markerCornersInteger[2].x - markerCornersInteger[0].x;
            int dy2 = markerCornersInteger[2].y - markerCornersInteger[0].y;
            int o = (dx1 * dy2) - (dy1 * dx2);

            if (o < 0) {
                swap(markerCornersInteger[1], markerCornersInteger[3]);
            }

            for (int i = 0; i < 4; i++) {
                markerCornersFloat.push_back(cv::Point2f(markerCornersInteger[i].x, markerCornersInteger[i].y));
            }

            Mat M = getPerspectiveTransform(markerCornersFloat, _markerPoints2D);

            warpPerspective(grayImage, *_canonicalMarker, M, cv::Size(kCanonicalMarkerSize, kCanonicalMarkerSize));

            threshold(*_canonicalMarker, *_canonicalMarker, 80, 255, THRESH_BINARY | THRESH_OTSU);


            newMarker = [[AMMarker alloc] init];
            _lastError = [AMArEngineHelpers getMarkerID:*_canonicalMarker markerOut:&newMarker];

            if (_lastError < 0) {
                if (drawBorder) {
                    [self drawDebugInfoForMarkerWithCorners:markerCornersFloat
                                                      image:realImage
                                                      color:errorColor];
                }
                return nil;
            }

            double initialRotation = newMarker.rotation;

            newMarker.size = [AMArEngineHelpers distanceWithPoint:markerCornersFloat[3] point2:markerCornersFloat[0]];
            newMarker.rotation += [AMArEngineHelpers computeRotation:markerCornersFloat[3] center:markerCornersFloat[0]
                                                                size:newMarker.size];

            Point2f center = Point2f(0, 0);
            for (unsigned int j = 0; j < 4; j++) {
                center.x += markerCornersFloat[j].x;
                center.y += markerCornersFloat[j].y;
            }
            center.x /= 4;
            center.y /= 4;

            newMarker.center = center;
            newMarker.lastVisible = [NSDate timeIntervalSinceReferenceDate];


            int rotationsNumber = (int) round(initialRotation / M_PI_2);

            for (int swaps = 0; swaps < rotationsNumber; swaps++) {
                swap(markerCornersFloat[0], markerCornersFloat[3]);
                swap(markerCornersFloat[0], markerCornersFloat[2]);
                swap(markerCornersFloat[0], markerCornersFloat[1]);
            }

            newMarker.imagePoints = markerCornersFloat;

            if (drawBorder) {
                [self drawDebugInfoForMarkerWithCorners:markerCornersFloat
                                                  image:realImage
                                                  color:correctColor];
            }
        }
    }

    return newMarker;
}

- (void)unwrapImageForMarkerWithCorners:(vector<cv::Point2f>)imagePoints inputImage:(Mat *)input outputImage:(Mat *)output {
    vector<cv::Point2f> currentPoints2D = _markerPoints2D;

    Matx33f M2 = getPerspectiveTransform(currentPoints2D, imagePoints);

    Point3f a = M2.inv() * Point3f(0, 0, 1);
    a = a * (1.0 / a.z);

    Point3f b = M2.inv() * Point3f(0, input->rows, 1);
    b = b * (1.0 / b.z);

    Point3f c = M2.inv() * Point3f(input->cols, input->rows, 1);
    c = c * (1.0 / c.z);

    Point3f d = M2.inv() * Point3f(input->cols, 0, 1);
    d = d * (1.0 / d.z);

    // to make sure all corners are in the image, every position must be > (0, 0)
    float x = (float) ceil(abs(min(min(a.x, b.x), min(c.x, d.x))));
    float y = (float) ceil(abs(min(min(a.y, b.y), min(c.y, d.y))));

    // and also < (width, height)
    float width = (float) (ceil(abs(max(max(a.x, b.x), max(c.x, d.x)))) + x);
    float height = (float) (ceil(abs(max(max(a.y, b.y), max(c.y, d.y)))) + y);

    // adjust target points accordingly
    for (int i = 0; i < 4; i++) {
        currentPoints2D[i] += Point2f(x, y);
    }

    // recalculate transformation
    M2 = getPerspectiveTransform(currentPoints2D, imagePoints);

    warpPerspective(*input, *output, M2, cv::Size(width, height), WARP_INVERSE_MAP);
}


- (void)drawDebugInfoForMarkerWithCorners:(vector<cv::Point2f>)imagePoints image:(Mat *)image color:(cv::Scalar)color {
    cv::line(*image, imagePoints[0], imagePoints[1], color, 4);
    cv::line(*image, imagePoints[1], imagePoints[2], color, 4);
    cv::line(*image, imagePoints[2], imagePoints[3], color, 4);
    cv::line(*image, imagePoints[0], imagePoints[3], color, 4);

    cv::putText(*image, "0", imagePoints[0], cv::FONT_HERSHEY_SIMPLEX, 0.3, CV_RGB(0, 0, 255));
    cv::putText(*image, "1", imagePoints[1], cv::FONT_HERSHEY_SIMPLEX, 0.3, CV_RGB(0, 255, 255));
    cv::putText(*image, "2", imagePoints[2], cv::FONT_HERSHEY_SIMPLEX, 0.3, CV_RGB(255, 255, 255));
    cv::putText(*image, "3", imagePoints[3], cv::FONT_HERSHEY_SIMPLEX, 0.3, CV_RGB(255, 0, 255));
}

@end
