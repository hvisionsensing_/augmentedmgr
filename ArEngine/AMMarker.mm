//
//  Created by Michał Mizera on 17.04.2013.
//


#import "AMMarker.h"
#import "AMArEngine.h"
#import "AMArEngineHelpers.h"
#import "AMConverter.h"

@implementation AMMarker
- (id)copyWithZone:(NSZone *)zone {
    AMMarker *newMarker = [[AMMarker allocWithZone:zone] init];
    if (newMarker) {
        newMarker.id = self.id;
        newMarker.center = self.center;
        newMarker.lastVisible = self.lastVisible;
        newMarker.size = self.size;
        newMarker.rotation = self.rotation;
        newMarker.fixPosition = self.fixPosition;
        newMarker.imagePoints = self.imagePoints;
    }

    return newMarker;
}

- (id)init {
    self = [super init];
    if (self) {
        _fixPosition = NO;
    }

    return self;
}

- (CGFloat)normalizeRotation:(CGFloat)angle {
    CGFloat returnAngle = angle;

    if (returnAngle < 0) {
        while (returnAngle < 0) {
            returnAngle += 2.0f * M_PI;
        }
    }
    else if (returnAngle >= 2.0f * M_PI) {
        while (returnAngle >= 2.0f * M_PI) {
            returnAngle -= 2.0f * M_PI;
        }
    }

    return returnAngle;
}

- (void)rotate:(CGFloat)angle pivot:(cv::Point2f)pivot {
    _center = [AMArEngineHelpers rotatePoint:_center withPivot:pivot byAngle:-angle];
    _rotation -= angle;
    _rotation = [self normalizeRotation:_rotation];
}

- (NSString *)markerDescription {
    NSString *description = [NSString stringWithFormat:@"Id %d Center: %f, %f  rotation: %f size: %f", _id,
                                                       _center.x, _center.y,
                                                       _rotation * 180.0f / M_PI, _size];
    return description;
}

- (NSString *)debugDescription {
    return [self markerDescription];
}

#pragma mark -
#pragma mark Serialization

- (NSDictionary *)dictRepresentation {
    NSMutableArray *imagePoints = [NSMutableArray array];

    for (int i = 0; i < _imagePoints.size(); i++) {
        Point2f point = _imagePoints[i];
        [imagePoints addObject:[AMConverter dictFromPoint2f:point]];
    }

    NSDictionary *center = [AMConverter dictFromPoint2f:_center];

    return @{
            @"id" : @(_id),
            @"center" : center,
            @"rotation" : @(_rotation),
            @"size" : @(_size),
            @"imagePoints" : imagePoints,
            @"lastVisible" : @(_lastVisible),
            @"fixPosition" : @(_fixPosition),
    };
}

- (NSString *)jsonRepresentation {
    NSData *serializedData = [NSJSONSerialization dataWithJSONObject:[self dictRepresentation]
                                                             options:nil
                                                               error:nil];
    NSString *serialized = [[NSString alloc] initWithData:serializedData encoding:NSUTF8StringEncoding];

    return serialized;
}

- (id)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _id = [dict[@"id"] intValue];
        _center = [AMConverter point2FfromDict:dict[@"center"]];
        _rotation = [dict[@"rotation"] floatValue];
        _size = [dict[@"size"] floatValue];
        _lastVisible = [dict[@"lastVisible"] doubleValue];
        _fixPosition = [dict[@"fixPosition"] boolValue];

        NSArray *serializedImagePoints = dict[@"imagePoints"];
        std::vector<cv::Point2f> tmpImagePoints;

        for (NSDictionary *imagePoint in serializedImagePoints) {
            cv::Point2f point = [AMConverter point2FfromDict:imagePoint];
            tmpImagePoints.push_back(point);
        }

        _imagePoints = tmpImagePoints;
    }

    return self;
}


@end
