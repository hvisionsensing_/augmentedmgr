#import <Objection/Objection.h>
#import "AMArEngineDelegate.h"
#import "AMArEngine.h"
#import "AMMarker.h"
#import "AMCalibrator.h"
#import "AMUserDefaultsManager.h"
#import "cap_ios.h"
#import "AMConverter.h"
#import "AMArEngineHelpers.h"
#import "AMMarkersDetector.h"
#import "AMCameraPoseDetector.h"
#import "AMCameraPose.h"

using namespace cv;
using namespace std;


@interface AMArEngine ()
@property(nonatomic, strong) AMUserDefaultsManager *userDefaultsManager;
@property(nonatomic, strong) AMCalibrator *calibrator;
@end


@implementation AMArEngine {
    AMMarkersDetector *_markersDetector;
    AMCameraPoseDetector *_cameraPoseDetector;

    NSMutableArray *_observers;
    CvVideoCamera *_camera;
}

objection_requires_sel(@selector(userDefaultsManager), @selector(calibrator))

- (void)awakeFromObjection {
    _mode = AMArEngineModeDisabled;
    _imageSize = CGSizeMake(0, 0);
    _observers = [NSMutableArray array];
    _camera = [[CvVideoCamera alloc] init];

    _camera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    _camera.defaultAVCaptureSessionPreset = [_userDefaultsManager videoQualityPreset];
    _camera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    _camera.defaultFPS = 30;
    _camera.delegate = self;

    [_camera start];
}


- (void)initializationWithImage:(cv::Mat &)image {
    _imageSize.width = image.cols;
    _imageSize.height = image.rows;

    _markersDetector = [[JSObjection defaultInjector] getObject:[AMMarkersDetector class]
                                                   argumentList:@[[NSValue valueWithCGSize:_imageSize]]];
    _cameraPoseDetector = [JSObjection defaultInjector][[AMCameraPoseDetector class]];
    _image = new cv::Mat((int) _imageSize.height, (int) _imageSize.width, CV_8UC3);

    _fieldOfView = [self calculateFieldOfViewWithImageSize:_imageSize];


    NSLog(@"_Camera imageSize %f x %f", _imageSize.width, _imageSize.height);
}

- (CGSize)calculateFieldOfViewWithImageSize:(CGSize)size {
    cv::Size imageSize((int) size.width, (int) size.height);
    double apertureWidth = 1;
    double apertureHeight = 1;
    double fieldOfViewX;
    double fieldOfViewY;
    double focalLength;
    Point2d principalPoint;
    double aspectRatio;
    calibrationMatrixValues([self currentCameraMatrix], imageSize, apertureWidth, apertureHeight, fieldOfViewX, fieldOfViewY, focalLength, principalPoint, aspectRatio);

    return CGSizeMake((CGFloat) fieldOfViewX, (CGFloat) fieldOfViewY);
}

- (void)addObserver:(id <AMArEngineDelegate>)observer {
    [_observers addObject:observer];
}

- (void)removeObserver:(id <AMArEngineDelegate>)observer {
    [_observers removeObject:observer];
}

- (cv::Mat)currentCameraMatrix {
    return [_userDefaultsManager cameraMatrixWithCurrentResolution:_imageSize];
}

- (void)setNextFrameReady {
    for (id <AMArEngineDelegate> observer in _observers) {
        if ([observer respondsToSelector:@selector(nextFrameReady)]) {
            [observer nextFrameReady];
        }
    }
}


- (void)processImage:(cv::Mat &)frame {
    @synchronized (self) {
        if (CGSizeEqualToSize(_imageSize, CGSizeMake(0, 0))) {
            [self initializationWithImage:frame];
        }

        switch (_mode) {
            case AMArEngineModeCalibrating:
                break;
            case AMArEngineModeMapGeneration:
                [_markersDetector markersInImage:&frame];
                break;
            case AMArEngineModeGameplay:
                [_markersDetector currentMarkersInImage:&frame];
                [_cameraPoseDetector calculateTotalCameraPose:_markersDetector.markers
                                                   markersMap:_userDefaultsManager.markersMap];
            case AMArEngineModeDisabled:
                break;
        }

    }

    cv::cvtColor(frame, *_image, CV_BGR2RGB);
    [self setNextFrameReady];
}

- (NSArray *)markers {
    @synchronized (self) {
        return [_markersDetector.markers copy];
    }
}

- (NSDictionary *)markersDict {
    @synchronized (self) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        for (AMMarker *marker in _markersDetector.markers) {
            dict[@(marker.id)] = [marker copy];
        }

        return dict;
    }
}

- (void)dealloc {
    [_camera stop];
}

- (Mat *)undistortedImage {
    return _markersDetector.undistortedImage;
}

- (AMCameraPose *)cameraPose {
    return _cameraPoseDetector.currentCameraPose;
}

@end
