//
//  Created by Michał Mizera on 03.11.2013.
//


#import <Foundation/Foundation.h>

@class AMMarker;
@class AMCameraPose;
@class AMMarkersMap;

using namespace cv;

@interface AMCameraPoseDetector : NSObject

@property(nonatomic, readonly) AMCameraPose *currentCameraPose;

- (AMCameraPose *)calculateCameraPoseForMarker:(AMMarker *)marker modelPoints:(vector<Point3d>)modelPoints;

- (void)calculateTotalCameraPose:(NSArray *)currentMarkers markersMap:(AMMarkersMap *)markersMap;
@end
