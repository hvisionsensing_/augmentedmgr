//
//  Created by Michał Mizera on 06.11.2013.
//


#import "AMCameraPose.h"
#import "AMConverter.h"

using namespace cv;

@implementation AMCameraPose {

}

- (id)init {
    self = [super init];
    if (self) {
        _rvec = Mat::zeros(3, 1, CV_64F);
        _tvec = Mat::zeros(3, 1, CV_64F);
    }

    return self;
}


- (cv::Mat)matrix {
    Mat rotation;
    Mat viewMatrix = Mat::eye(4, 4, CV_64F);
    Rodrigues(_rvec, rotation);

    for (unsigned int row = 0; row < 3; ++row) {
        for (unsigned int col = 0; col < 3; ++col) {
            viewMatrix.at<double>(row, col) = rotation.at<double>(row, col);
        }
        viewMatrix.at<double>(row, 3) = _tvec.at<double>(row, 0);
    }

    Mat rotateX180 = [AMConverter rotationMatrixWithX:[AMConverter fromDegreesToRadians:180] Y:0
                                                    Z:0];
    viewMatrix = viewMatrix * rotateX180;

    return viewMatrix;
}

@end
