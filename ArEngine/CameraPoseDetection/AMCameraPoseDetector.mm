//
//  Created by Michał Mizera on 03.11.2013.
//


#import "AMCameraPoseDetector.h"
#import <Objection/Objection.h>
#import "AMMarkersDetector.h"
#import "imgproc.hpp"
#import "AMMarker.h"
#import "AMCameraPose.h"
#import "core.hpp"
#import "AMMarker.h"
#import "AMConverter.h"
#import "AMUserDefaultsManager.h"
#import "AMArEngineHelpers.h"
#import "AMArEngine.h"
#import "AMCameraPose.h"
#import "NSDictionary+Empty.h"
#import "NSArray+Empty.h"
#import "AMMarkersMap.h"

@interface AMCameraPoseDetector ()
@property(nonatomic) AMUserDefaultsManager *userDefaultsManager;
@property(nonatomic) AMArEngine *arEngine;
@end

@implementation AMCameraPoseDetector {

}

objection_requires_sel(@selector(userDefaultsManager), @selector(arEngine))

- (void)awakeFromObjection {
    [super awakeFromObjection];

    _currentCameraPose = [[AMCameraPose alloc] init];
}


- (AMCameraPose *)calculateCameraPoseForMarker:(AMMarker *)marker modelPoints:(vector<Point3d>)modelPoints {
    AMCameraPose *cameraPose = [[AMCameraPose alloc] init];
    Mat rvec, tvec;

    solvePnP(modelPoints, marker.imagePoints, [_arEngine currentCameraMatrix], _userDefaultsManager.distCoeffs, rvec, tvec);

    cv::Mat R;
    cv::Rodrigues(rvec, R);

    R = R.t();
    cameraPose.tvecInv = -R * tvec;

    cameraPose.rvec = rvec;
    cameraPose.tvec = tvec;

    return cameraPose;
}

- (void)calculateTotalCameraPose:(NSArray *)currentMarkers markersMap:(AMMarkersMap *)markersMap {
    if (!markersMap || !currentMarkers || [markersMap isEmpty] || [currentMarkers isEmpty]) return;

    AMCameraPose *cameraPose = [[AMCameraPose alloc] init];
    CGFloat realMarkerSize = _userDefaultsManager.markerSize;

    for (AMMarker *marker in currentMarkers) {
        AMMarker *mapMarker = markersMap[@(marker.id)];
        std::vector <cv::Point3d> markerPoints3D;

        markerPoints3D.push_back(cv::Point3d(-realMarkerSize / 2.0f, -realMarkerSize / 2.0f, 0));
        markerPoints3D.push_back(cv::Point3d(realMarkerSize / 2.0f, -realMarkerSize / 2.0f, 0));
        markerPoints3D.push_back(cv::Point3d(realMarkerSize / 2.0f, realMarkerSize / 2.0f, 0));
        markerPoints3D.push_back(cv::Point3d(-realMarkerSize / 2.0f, realMarkerSize / 2.0f, 0));

        for (int i = 0; i < 4; i++) {
            cv::Point2f point = cv::Point2f(markerPoints3D[i].x, markerPoints3D[i].y);

            point = [AMArEngineHelpers rotatePoint:point withPivot:cv::Point2f(0, 0) byAngle:mapMarker.rotation];
            point += mapMarker.center * realMarkerSize;

            markerPoints3D[i].x = point.x;
            markerPoints3D[i].y = point.y;
        }

        AMCameraPose *currentPose = [self calculateCameraPoseForMarker:marker modelPoints:markerPoints3D];

        cameraPose.rvec += currentPose.rvec * (1.0 / (float)currentMarkers.count);
        cameraPose.tvec += currentPose.tvec * (1.0 / (float)currentMarkers.count);
        cameraPose.tvecInv += currentPose.tvecInv * (1.0 / (float)currentMarkers.count);

    }

    _currentCameraPose = cameraPose;
}

@end
