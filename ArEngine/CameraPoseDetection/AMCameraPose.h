//
//  Created by Michał Mizera on 06.11.2013.
//


#import <Foundation/Foundation.h>
#import "core.hpp"

using namespace std;
using namespace cv;

@interface AMCameraPose : NSObject

@property(nonatomic) Mat tvec;
@property(nonatomic) Mat rvec;

@property(nonatomic) Mat tvecInv;

- (cv::Mat)matrix;
@end
