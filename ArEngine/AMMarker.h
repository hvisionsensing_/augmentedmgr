//
//  Created by Michał Mizera on 17.04.2013.
//

#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>

@interface AMMarker : NSObject <NSCopying>
@property(nonatomic) NSInteger id;
@property(nonatomic) cv::Point2f center;
@property(nonatomic) CGFloat rotation;
@property(nonatomic) CGFloat size;
@property(nonatomic) std::vector<cv::Point2f> imagePoints;
@property(nonatomic) NSTimeInterval lastVisible;
@property(nonatomic) BOOL fixPosition;

- (id)initWithDict:(NSDictionary *)dict;

- (void)rotate:(CGFloat)angle pivot:(cv::Point2f)pivot;

- (NSString *)markerDescription;

- (NSString *)jsonRepresentation;
@end
