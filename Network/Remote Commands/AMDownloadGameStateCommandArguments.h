//
//  Created by Michał Mizera on 16.11.2013.
//


#import <Foundation/Foundation.h>
#import <threeMF/TMFKeyValueCommand.h>

@interface AMDownloadGameStateCommandArguments : TMFKeyValueCommandArguments
@end
