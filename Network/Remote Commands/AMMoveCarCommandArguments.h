//
//  Created by Michał Mizera on 17.11.2013.
//


#import <Foundation/Foundation.h>
#import "TMFAnnounceCommand.h"

@interface AMMoveCarCommandArguments : TMFAnnounceCommandArguments
@property(nonatomic) CGFloat x;
@property(nonatomic) CGFloat y;
@end
