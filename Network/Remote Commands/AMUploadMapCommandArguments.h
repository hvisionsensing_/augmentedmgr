//
//  Created by Michał Mizera on 09.11.2013.
//


#import <Foundation/Foundation.h>
#import "TMFAnnounceCommand.h"


@interface AMUploadMapCommandArguments : TMFAnnounceCommandArguments
@property (nonatomic) NSString * serializedMap;
@end
