//
//  Created by Michał Mizera on 16.11.2013.
//
#import <Foundation/Foundation.h>
#import "TMFKeyValueCommand.h"

typedef NS_ENUM(NSInteger, MSPhysicObjectType) {
    MSPhysicObjectTypeTree,
    MSPhysicObjectTypeBox,
    MSPhysicObjectTypeRock,
    MSPhysicObjectTypeCone,

    MSPhysicObjectTypeCar1,
    MSPhysicObjectTypeCar2,
    MSPhysicObjectTypeCar3,
    MSPhysicObjectTypeCar4,
};


extern NSString *MSPhysicObjectIdIdentifier;
extern NSString *MSPhysicObjectTypeIdentifier;
extern NSString *MSPhysicObjectXPosIdentifier;
extern NSString *MSPhysicObjectYPosIdentifier;
extern NSString *MSPhysicObjectAngleIdentifier;

@interface AMDownloadGameStateCommand : TMFKeyValueCommand
@end
