//
//  Created by Michał Mizera on 31.07.13.
//


#import <AVFoundation/AVFoundation.h>
#import "AMUserDefaultsManager.h"
#import "core.hpp"
#import "NSString+Matrix.h"
#import "AMMarker.h"
#import "AMMarkersMap.h"

NSString *AMUserDefaultsManagerCameraMatrixIdentifier = @"AMUserDefaultsManagerCameraMatrixIdentifier";
NSString *AMUserDefaultsManagerCameraMatrixImageSizeIdentifier = @"AMUserDefaultsManagerCameraMatrixImageSizeIdentifier";
NSString *AMUserDefaultsManagerDistCoeffsIdentifier = @"AMUserDefaultsManagerDistCoeffsIdentifier";
NSString *AMUserDefaultsManagerVideoQualityPreset = @"AMUserDefaultsManagerVideoQualityPreset";
NSString *AMUserDefaultsManagerMarkersMapIdentifier = @"AMUserDefaultsManagerMarkersMapIdentifier";
NSString *AMUserDefaultsManagerMarkerSize = @"AMUserDefaultsManagerMarkerSize";


@implementation AMUserDefaultsManager {
    NSUserDefaults *_userDefaults;
}

- (void)awakeFromObjection {
    _userDefaults = [NSUserDefaults standardUserDefaults];
    _markersMap = [[AMMarkersMap alloc] init];
    [self loadMap];
}

- (void)clear {
    [_userDefaults setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
}

#pragma mark -
#pragma mark Properties


- (cv::Mat)cameraMatrixWithCurrentResolution:(CGSize)size {
    NSDictionary *dictionary = [_userDefaults dictionaryRepresentation];
    if (![[dictionary allKeys] containsObject:AMUserDefaultsManagerCameraMatrixIdentifier]) {
        cv::Mat emptyCameraMatrix = cv::Mat::eye(3, 3, CV_64F);
        return emptyCameraMatrix;
    }

    cv::Mat tmpCameraMatrix = cv::Mat::eye(3, 3, CV_64F);
    NSArray *cameraArray = [_userDefaults objectForKey:AMUserDefaultsManagerCameraMatrixIdentifier];

    CGSize savedSize = size;
    if ([[dictionary allKeys] containsObject:AMUserDefaultsManagerCameraMatrixImageSizeIdentifier]) {
        savedSize = CGSizeFromString([_userDefaults objectForKey:AMUserDefaultsManagerCameraMatrixImageSizeIdentifier]);
    }

    for (NSUInteger i = 0; i < 3; i++) {
        for (NSUInteger j = 0; j < 3; j++) {
            tmpCameraMatrix.at<double>(i, j) = [cameraArray[i * 3 + j] doubleValue];
        }
    }

    CGFloat aspectX = size.width / savedSize.width;
    CGFloat aspectY = size.height / savedSize.height;

    tmpCameraMatrix.at<double>(0, 0) *= aspectX; // fx
    tmpCameraMatrix.at<double>(1, 1) *= aspectY; // fy
    tmpCameraMatrix.at<double>(0, 2) *= aspectX; // cx
    tmpCameraMatrix.at<double>(1, 2) *= aspectY; // cy

    return tmpCameraMatrix;
}

- (CGFloat)markerSize {
    if (![[[_userDefaults dictionaryRepresentation] allKeys] containsObject:AMUserDefaultsManagerMarkerSize]) {
        return 18.0f;
    }

    NSString *markerSize = [_userDefaults stringForKey:AMUserDefaultsManagerMarkerSize];
    return [markerSize floatValue];
}

- (void)setCameraMatrix:(cv::Mat)cameraMatrix withResolution:(CGSize)size {
    NSMutableArray *cameraArray = [NSMutableArray array];

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            double value = cameraMatrix.at<double>(i, j);
            [cameraArray addObject:@(value)];
        }
    }

    NSLog(@"STARE cameraArray:\n%@", [NSString stringFromMat:[self cameraMatrixWithCurrentResolution:size]]);
    NSLog(@"NOWE cameraArray:\n%@", [NSString stringFromMat:cameraMatrix]);

    [_userDefaults setObject:cameraArray forKey:AMUserDefaultsManagerCameraMatrixIdentifier];
    [_userDefaults setObject:NSStringFromCGSize(size)
                      forKey:AMUserDefaultsManagerCameraMatrixImageSizeIdentifier];

}

- (cv::Mat)distCoeffs {
    if (![[[_userDefaults dictionaryRepresentation] allKeys] containsObject:AMUserDefaultsManagerDistCoeffsIdentifier]) {
        cv::Mat emptyDistCoeffs = cv::Mat::zeros(8, 1, CV_64F);
        return emptyDistCoeffs;
    }

    NSArray *coeffsArray = [_userDefaults objectForKey:AMUserDefaultsManagerDistCoeffsIdentifier];
    cv::Mat tmpCoeffs = cv::Mat::zeros([coeffsArray count], 1, CV_64F);

    for (NSUInteger i = 0; i < [coeffsArray count]; i++) {
        tmpCoeffs.at<double>(i) = [coeffsArray[i] doubleValue];
    }

    return tmpCoeffs;
}

- (void)setDistCoeffs:(cv::Mat)distCoeffs {
    NSMutableArray *coeffsArray = [NSMutableArray array];

    for (NSUInteger i = 0; i < distCoeffs.rows; i++) {
        [coeffsArray addObject:@((distCoeffs.at<double>(i)))];
    }

    NSLog(@"STARE coeffsArray:\n%@", [NSString stringFromMat:[self distCoeffs]]);
    NSLog(@"NOWE coeffsArray:\n%@", [NSString stringFromMat:distCoeffs]);

    [_userDefaults setObject:coeffsArray forKey:AMUserDefaultsManagerDistCoeffsIdentifier];
}

- (NSString *)videoQualityPreset {
    if (![[[_userDefaults dictionaryRepresentation] allKeys] containsObject:AMUserDefaultsManagerVideoQualityPreset]) {
        return AVCaptureSessionPresetMedium;
    }

    return [_userDefaults stringForKey:AMUserDefaultsManagerVideoQualityPreset];
}

#pragma mark -
#pragma mark Markers map

- (void)clearMarkers {
    [_markersMap.markersMap removeAllObjects];
}

- (void)saveMarkerMap {
    [_userDefaults setObject:_markersMap.serializedMap forKey:AMUserDefaultsManagerMarkersMapIdentifier];
    [_userDefaults synchronize];
}

- (void)loadMap {
    NSString *serializedMap = [_userDefaults stringForKey:AMUserDefaultsManagerMarkersMapIdentifier];
    [_markersMap loadMap:serializedMap];
}


@end
