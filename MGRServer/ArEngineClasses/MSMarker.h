//
//  Created by Michał Mizera on 17.04.2013.
//

#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>

@interface MSMarker : NSObject <NSCopying>
@property(nonatomic) NSInteger id;
@property(nonatomic) CGPoint center;
@property(nonatomic) CGFloat rotation;
@property(nonatomic) CGFloat size;
@property(nonatomic) NSTimeInterval lastVisible;
@property(nonatomic) BOOL fixPosition;

- (id)initWithDict:(NSDictionary *)dict;

- (NSString *)markerDescription;

- (NSString *)jsonRepresentation;
@end
