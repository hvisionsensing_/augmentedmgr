//
//  MSMasterViewController.h
//  MGRServer
//
//  Created by Michal Mizera on 09.11.2013.
//  Copyright (c) 2013 Michal Mizera. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "threeMF.h"

@class MSMarkerMapView;

@interface MSMasterViewController : NSViewController <TMFConnectorDelegate, NSWindowDelegate, NSTableViewDelegate, NSTableViewDataSource>

@property (strong) IBOutlet MSMarkerMapView *markersMapView;
@property (strong) IBOutlet NSView *physicView;
@property (strong) IBOutlet NSTableView *currentUsersTable;
@property (strong) IBOutlet NSScrollView *contentScrollView;

@end
