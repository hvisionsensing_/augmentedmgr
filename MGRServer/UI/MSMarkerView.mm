//
//  Created by Michał Mizera on 11.11.2013.
//


#import "MSMarkerView.h"


@implementation MSMarkerView {
    NSImageView *_background;
    NSTextView *_numberLabel;
}

- (id)initWithFrame:(NSRect)frameRect markerId:(NSInteger)markerId {
    self = [super initWithFrame:frameRect];
    if (self) {
        _background = [[NSImageView alloc] initWithFrame:frameRect];
        _background.translatesAutoresizingMaskIntoConstraints = NO;
        _background.image = [NSImage imageNamed:@"marker"];
        [self addSubview:_background];


        _numberLabel = [[NSTextView alloc] initWithFrame:frameRect];
        _numberLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _numberLabel.backgroundColor = [NSColor clearColor];
        _numberLabel.font = [NSFont boldSystemFontOfSize:11];
        _numberLabel.textColor = [NSColor redColor];
        [_numberLabel setAlignment:NSCenterTextAlignment];
        _numberLabel.string = [NSString stringWithFormat:@"%li", markerId];
        [self addSubview:_numberLabel];
    }

    return self;
}


- (void)resizeSubviewsWithOldSize:(NSSize)oldSize {
    [super resizeSubviewsWithOldSize:oldSize];

    _background.frame = self.bounds;

    CGSize size = [_numberLabel.string sizeWithAttributes:@{NSFontAttributeName : _numberLabel.font}];
    CGRect numberLabelFrame = CGRectMake(0, (NSHeight(self.bounds) - size.height) / 2.0, self.bounds.size.width, size.height);
    _numberLabel.frame = numberLabelFrame;

}

@end
