//
//  Created by Michał Mizera on 11.11.2013.
//


#import <Foundation/Foundation.h>

@class MSMarkersMap;
extern NSInteger kMarkerViewSize;

@interface MSMarkerMapView : NSView

- (void)setupMarkersViewsWithMap:(MSMarkersMap *)aMarkersMap;

@end
