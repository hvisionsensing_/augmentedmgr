//
//  MSMasterViewController.m
//  MGRServer
//
//  Created by Michal Mizera on 09.11.2013.
//  Copyright (c) 2013 Michal Mizera. All rights reserved.
//

#import "MSMasterViewController.h"
#import "TMFConnector.h"
#import "TMFAnnounceCommand.h"
#import "AMUploadMapCommand.h"
#import "AMUploadMapCommandArguments.h"
#import "MSMarkersMap.h"
#import "AMDownloadMapCommand.h"
#import "MSMarkerMapView.h"
#import "MSMarker.h"
#import "MSPhysicViewController.h"
#import "AMDownloadGameStateCommand.h"
#import "AMStartGameCommand.h"
#import "AMStartGameCommandArguments.h"
#import "AMMoveCarCommand.h"
#import "AMMoveCarCommandArguments.h"
#import "MSHelpers.h"

@interface MSMasterViewController ()

@end

@implementation MSMasterViewController {
    TMFConnector *_connector;
    MSMarkersMap *_markersMap;

    AMUploadMapCommand *_uploadMapCommand;
    AMDownloadMapCommand *_downloadMapCommand;

    MSPhysicViewController *_physicViewController;
    NSMutableArray *_activeUsers;
    AMDownloadGameStateCommand *_downloadGameStateCommand;
    AMStartGameCommand *_startGameCommand;
    AMMoveCarCommand *_moveCarCommand;
    NSView *_documentView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _connector = [TMFConnector new];
        _connector.delegate = self;
        _activeUsers = [NSMutableArray array];
        _physicViewController = [[MSPhysicViewController alloc] initWithNibName:@"MSPhysicViewController" bundle:nil];

        _uploadMapCommand = [[AMUploadMapCommand alloc] initWithRequestReceivedBlock:^(AMUploadMapCommandArguments *arguments, TMFPeer *peer, responseBlock_t responseBlock) {
            _markersMap = [[MSMarkersMap alloc] init];
            [_markersMap loadMap:arguments.serializedMap];

            dispatch_async(dispatch_get_main_queue(), ^{
                [_markersMapView setupMarkersViewsWithMap:_markersMap];
            });

            responseBlock(nil, nil);
        }];

        _downloadMapCommand = [[AMDownloadMapCommand alloc] initWithRequestReceivedBlock:^(TMFAnnounceCommandArguments *arguments, TMFPeer *peer, responseBlock_t responseBlock) {
            NSString *serializedMap = _markersMap ? _markersMap.serializedMap : @"";

            responseBlock(serializedMap, nil);
        }];

        _downloadGameStateCommand = [AMDownloadGameStateCommand new];
        _physicViewController.gameStateUpdateCommand = _downloadGameStateCommand;

        _startGameCommand = [[AMStartGameCommand alloc] initWithRequestReceivedBlock:^(AMStartGameCommandArguments *arguments, TMFPeer *peer, responseBlock_t responseBlock) {
            NSPoint cameraPosition = CGPointMake(arguments.x, arguments.y);
            NSPoint closestMarkerPosition = [self markerPositionClosestToPoint:cameraPosition];
            [_physicViewController addPlayer:peer AtPosition:closestMarkerPosition];

            responseBlock(nil, nil);
        }];

        _moveCarCommand = [[AMMoveCarCommand alloc] initWithRequestReceivedBlock:^(AMMoveCarCommandArguments *arguments, TMFPeer *peer, responseBlock_t responseBlock) {
            NSPoint position = CGPointMake(arguments.x, arguments.y);
            [_physicViewController movePlayer:peer withPosition:position];

            responseBlock(nil, nil);
        }];

        [_connector publishCommands:@[_uploadMapCommand, _downloadMapCommand, _downloadGameStateCommand, _startGameCommand, _moveCarCommand]];
    }
    return self;
}

- (NSPoint)markerPositionClosestToPoint:(NSPoint)point {
    CGFloat closestDistance = CGFLOAT_MAX;
    MSMarker *closestMarker;

    for (MSMarker *marker in _markersMap.allValues) {
        CGFloat currentDistance = [MSHelpers distanceBetweenPoints:marker.center pointB:point];

        if (currentDistance < closestDistance) {
            closestDistance = currentDistance;
            closestMarker = marker;
        }

    }

    return closestMarker.center;
}

- (IBAction)resetButtonPressed:(id)sender {
    [_physicViewController reset];
}

- (void)connector:(TMFConnector *)connector didAddSubscriber:(TMFPeer *)peer toCommand:(TMFPublishSubscribeCommand *)command {
    [_activeUsers addObject:peer];
    [_currentUsersTable reloadData];
}

- (void)connector:(TMFConnector *)connector didRemoveSubscriber:(TMFPeer *)peer fromCommand:(TMFPublishSubscribeCommand *)command {
    [_activeUsers removeObject:peer];
    [_currentUsersTable reloadData];
}

- (void)loadView {
    [super loadView];

    CGRect bigFrame = CGRectMake(0, 0, 3000, 3000);
    _documentView = [[NSView alloc] initWithFrame:bigFrame];
    [_contentScrollView setDocumentView:_documentView];

    _markersMapView = [[MSMarkerMapView alloc] initWithFrame:bigFrame];
    [_documentView addSubview:_markersMapView];

    _physicView = _physicViewController.view;
    [_documentView addSubview:_physicView];
    _physicView.frame = _documentView.frame;

    _currentUsersTable.delegate = self;
    _currentUsersTable.dataSource = self;

    [self scrollToCenter:_contentScrollView];
}

- (void)scrollToCenter:(NSScrollView *)scrollView {
    const CGFloat midX = NSMidX([[scrollView documentView] bounds]);
    const CGFloat midY = NSMidY([[scrollView documentView] bounds]);

    const CGFloat halfWidth = NSWidth([[scrollView contentView] frame]) / 2.0;
    const CGFloat halfHeight = NSHeight([[scrollView contentView] frame]) / 2.0;

    NSPoint newOrigin = NSMakePoint(midX - halfWidth, midY - halfHeight);
    [[scrollView documentView] scrollPoint:newOrigin];
}

#pragma mark -
#pragma mark NSWindowDelegate

- (void)windowDidResize:(NSNotification *)notification {

}

- (void)windowDidUpdate:(NSNotification *)notification {
    [self.markersMapView setNeedsLayout:YES];
    _physicView.frame = _documentView.frame;
    _markersMapView.frame = _documentView.frame;
}

#pragma mark -
#pragma mark NSTableView delegate & data source

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [_activeUsers count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    TMFPeer *user = _activeUsers[(NSUInteger) row];

    return [user name];
}

@end
