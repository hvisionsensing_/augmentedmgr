//
//  Created by Michał Mizera on 13.11.2013.
//

#import "MSPhysicObject.h"
#include "b2World.h"
#import "MSPhysicViewController.h"
#import "MSMarkerMapView.h"
#include "b2Body.h"
#include "b2Fixture.h"

NSString *kMSPhysicObjectImageIdentifier = @"image";
NSString *kMSPhysicObjectDensityIdentifier = @"density";
NSString *kMSPhysicObjectSizeIdentifier = @"size";
NSString *kMSPhysicObjectBodyTypeIdentifier = @"bodyType";
NSString *kMSPhysicObjectTypeIdentifier = @"type";

@implementation MSPhysicObject {MSPhysicObjectType _type;}

+ (instancetype)physicObjectWithType:(MSPhysicObjectType)type position:(NSPoint)position world:(b2World *)world {
    static NSDictionary *objectsLibrary = @{
            @(MSPhysicObjectTypeTree) : @{
                    kMSPhysicObjectImageIdentifier : @"tree",
                    kMSPhysicObjectDensityIdentifier : @1.0f,
                    kMSPhysicObjectSizeIdentifier : @2.0f,
                    kMSPhysicObjectBodyTypeIdentifier : @(b2_kinematicBody)},
            @(MSPhysicObjectTypeRock) : @{
                    kMSPhysicObjectImageIdentifier : @"stone",
                    kMSPhysicObjectDensityIdentifier : @1.0f,
                    kMSPhysicObjectSizeIdentifier : @0.35f,
                    kMSPhysicObjectBodyTypeIdentifier : @(b2_kinematicBody)},
            @(MSPhysicObjectTypeBox) : @{
                    kMSPhysicObjectImageIdentifier : @"box",
                    kMSPhysicObjectDensityIdentifier : @10.0f,
                    kMSPhysicObjectSizeIdentifier : @0.4f,
                    kMSPhysicObjectBodyTypeIdentifier : @(b2_dynamicBody)},
            @(MSPhysicObjectTypeCone) : @{
                    kMSPhysicObjectImageIdentifier : @"cone",
                    kMSPhysicObjectDensityIdentifier : @10.0f,
                    kMSPhysicObjectSizeIdentifier : @0.3f,
                    kMSPhysicObjectBodyTypeIdentifier : @(b2_dynamicBody)},
            @(MSPhysicObjectTypeCar1) : @{
                    kMSPhysicObjectImageIdentifier : @"car1",
                    kMSPhysicObjectDensityIdentifier : @2.0f,
                    kMSPhysicObjectSizeIdentifier : @1.0f,
                    kMSPhysicObjectBodyTypeIdentifier : @(b2_dynamicBody)},
            @(MSPhysicObjectTypeCar2) : @{
                    kMSPhysicObjectImageIdentifier : @"car2",
                    kMSPhysicObjectDensityIdentifier : @2.0f,
                    kMSPhysicObjectSizeIdentifier : @1.0f,
                    kMSPhysicObjectBodyTypeIdentifier : @(b2_dynamicBody)},
            @(MSPhysicObjectTypeCar3) : @{
                    kMSPhysicObjectImageIdentifier : @"car3",
                    kMSPhysicObjectDensityIdentifier : @2.0f,
                    kMSPhysicObjectSizeIdentifier : @1.0f,
                    kMSPhysicObjectBodyTypeIdentifier : @(b2_dynamicBody)},
            @(MSPhysicObjectTypeCar4) : @{
                    kMSPhysicObjectImageIdentifier : @"car4",
                    kMSPhysicObjectDensityIdentifier : @2.0f,
                    kMSPhysicObjectSizeIdentifier : @1.0f,
                    kMSPhysicObjectBodyTypeIdentifier : @(b2_dynamicBody)},

    };

    NSMutableDictionary *currentObject = [objectsLibrary[@(type)] mutableCopy];
    currentObject[kMSPhysicObjectTypeIdentifier] = @(type);

    return [[MSPhysicObject alloc] initObjectDict:currentObject
                                            world:world
                                         position:position];
}


- (instancetype)initObjectDict:(NSDictionary *)objectDict world:(b2World *)world position:(NSPoint)position {

    static NSInteger objectsCounter = 0;
    _objectId = objectsCounter++;

    _type = (MSPhysicObjectType) [objectDict[kMSPhysicObjectTypeIdentifier] intValue];

    NSImage *image = [NSImage imageNamed:objectDict[kMSPhysicObjectImageIdentifier]];

    CGFloat largerSideSize = [objectDict[kMSPhysicObjectSizeIdentifier] floatValue] * kMarkerViewSize;
    CGSize size = image.size;

    CGFloat aspect = size.width / size.height;

    if (size.width > size.height) {
        size.width = largerSideSize;
        size.height = size.width / aspect;
    } else {
        size.height = largerSideSize;
        size.width = size.height * aspect;
    }

    self = [super initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    if (self) {

        float density = [objectDict[kMSPhysicObjectDensityIdentifier] floatValue];
        b2BodyType bodyType = (b2BodyType) [objectDict[kMSPhysicObjectBodyTypeIdentifier] intValue];
        CGSize halfSize;
        halfSize.width = size.width / (kMarkerViewSize * PHYSIC_SCALE * 2.0f);
        halfSize.height = size.height / (kMarkerViewSize * PHYSIC_SCALE * 2.0f);
        position.x /= kMarkerViewSize * PHYSIC_SCALE;
        position.y /= kMarkerViewSize * PHYSIC_SCALE;

        b2BodyDef bodyDef;
        bodyDef.type = bodyType;
        bodyDef.position.Set((float32) position.x, (float32) position.y);
        _body = world->CreateBody(&bodyDef);
        _shape = new b2PolygonShape();
        _shape->SetAsBox((float32) halfSize.width, (float32) halfSize.height);
        _fixture = _body->CreateFixture(_shape, density);
        _fixture->SetRestitution(0.1);
        _body->SetLinearDamping(5);
        _body->SetAngularDamping(5);

        self.image = image;
    }

    return self;
}

- (void)drawRect:(NSRect)dirtyRect {
    // set any NSColor for filling, say white:
    [[NSColor whiteColor] setFill];
    NSRectFill(dirtyRect);
    [super drawRect:dirtyRect];
}

- (NSDictionary *)dictRepresentation {
    return @{
            MSPhysicObjectIdIdentifier : @(_objectId),
            MSPhysicObjectTypeIdentifier : @(_type),
            MSPhysicObjectXPosIdentifier : @((_body->GetPosition().x * PHYSIC_SCALE)),
            MSPhysicObjectYPosIdentifier : @((_body->GetPosition().y * PHYSIC_SCALE)),
            MSPhysicObjectAngleIdentifier : @(_body->GetAngle()),

    };
}

@end
