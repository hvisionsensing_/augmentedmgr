//
//  MSAppDelegate.m
//  MGRServer
//
//  Created by Michal Mizera on 09.11.2013.
//  Copyright (c) 2013 Michal Mizera. All rights reserved.
//

#import "MSAppDelegate.h"

@implementation MSAppDelegate {
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    _masterViewController = [[MSMasterViewController alloc] initWithNibName:@"MSMasterViewController" bundle:nil];
    self.window.contentView = _masterViewController.view;
    _masterViewController.view.frame = ((NSView *) self.window.contentView).bounds;

    self.window.delegate = _masterViewController;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
    return YES;
}

@end
