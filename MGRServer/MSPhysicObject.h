//
//  Created by Michał Mizera on 13.11.2013.
//


#import <Foundation/Foundation.h>
#import <box2d/Dynamics/b2World.h>
#import <box2d/Collision/Shapes/b2PolygonShape.h>
#import <box2d/Dynamics/b2Body.h>
#import <box2d/Dynamics/b2Fixture.h>
#import "AMDownloadGameStateCommand.h"

@interface MSPhysicObject : NSImageView


@property(nonatomic) b2PolygonShape *shape;
@property(nonatomic) b2Body *body;
@property(nonatomic) b2FixtureDef *fixtureDef;
@property(nonatomic) b2Fixture *fixture;
@property(nonatomic) NSInteger objectId;

+ (instancetype)physicObjectWithType:(MSPhysicObjectType)type position:(NSPoint)position world:(b2World *)world;

- (NSDictionary *)dictRepresentation;
@end
