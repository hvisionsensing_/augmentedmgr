
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// OpenCV
#define COCOAPODS_POD_AVAILABLE_OpenCV
#define COCOAPODS_VERSION_MAJOR_OpenCV 2
#define COCOAPODS_VERSION_MINOR_OpenCV 4
#define COCOAPODS_VERSION_PATCH_OpenCV 6

// PLVisualAttributeConstraints
#define COCOAPODS_POD_AVAILABLE_PLVisualAttributeConstraints
#define COCOAPODS_VERSION_MAJOR_PLVisualAttributeConstraints 1
#define COCOAPODS_VERSION_MINOR_PLVisualAttributeConstraints 0
#define COCOAPODS_VERSION_PATCH_PLVisualAttributeConstraints 1

// threeMF
#define COCOAPODS_POD_AVAILABLE_threeMF
#define COCOAPODS_VERSION_MAJOR_threeMF 0
#define COCOAPODS_VERSION_MINOR_threeMF 1
#define COCOAPODS_VERSION_PATCH_threeMF 0

