//
//  Created by Michał Mizera on 06.11.2013.
//


#import <Foundation/Foundation.h>

@interface NSDictionary (Empty)
- (BOOL)isEmpty;
@end
