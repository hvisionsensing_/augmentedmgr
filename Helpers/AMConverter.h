//
//  Created by Michał Mizera on 13.10.2013.
//


#import <Foundation/Foundation.h>

#import "cap_ios.h"
#import <NinevehGL/NinevehGL.h>

@class AMCameraPose;

@interface AMConverter : NSObject
+ (NSDictionary *)dictFromPoint2f:(cv::Point2f)point2f;

+ (cv::Point2f)point2FfromDict:(NSDictionary *)dictionary;

+ (NGLvec3 *)nglVectorFromOpenCV:(cv::Point3f *)vec;

+ (NGLvec3 *)translateNGLVectorFromRadiansToDegrees:(NGLvec3 *)vector;

+ (NGLvec3 *)translateNGLVectorFromDegreesToRadians:(NGLvec3 *)vector;

+ (cv::Point3f *)translateOpenCVVectorFromRadiansToDegrees:(cv::Point3f *)vector;

+ (cv::Point3f *)translateOpenCVVectorFromDegreesToRadians:(cv::Point3f *)vector;

+ (float)fromRadiansToDegrees:(float)radians;

+ (float)fromDegreesToRadians:(float)degrees;

+ (NSArray *)arrayFromMat:(cv::Mat)mat;

+ (void)nglMatFromOpenCVMat:(cv::Mat)mat nglMat:(NGLmat4 *)output;

+ (NSString *)strinfFromNGLMatrix:(NGLmat4 *)mat;

+ (cv::Mat)translationMatrixWithX:(double)x Y:(double)y Z:(double)z;

+ (cv::Mat)scaleMatrixWithX:(double)x Y:(double)y Z:(double)z;

+ (cv::Mat)rotationMatrixWithX:(double)x Y:(double)y Z:(double)z;

+ (cv::Mat)rotationMatrix33WithX:(double)x Y:(double)y Z:(double)z;

+ (CGFloat)calculateDistance:(AMCameraPose *)pose;
@end
