#import <Objection/JSObjectionUtils.h>

#define objection_initializer_sel(selectorSymbol, args...) \
    + (NSDictionary *)objectionInitializer { \
        id objs[]= {args}; \
        NSArray *defaultArguments = [NSArray arrayWithObjects: objs count:sizeof(objs)/sizeof(id)]; \
        return JSObjectionUtils.buildInitializer(selectorSymbol, defaultArguments); \
    }

inline static CGFloat lerp(CGFloat val1, CGFloat val2, CGFloat percent) {
    return val1 + percent * (val2 - val1);
}
