//
//  Created by Michał Mizera on 09.10.2013.
//


#import "NSString+Matrix.h"


@implementation NSString (Matrix)

+ (NSString *)stringFromMat:(cv::Mat)mat {
    NSMutableString *string = [NSMutableString stringWithFormat:@""];

    for (int x = 0; x < mat.cols; x++) {
        for (int y = 0; y < mat.rows; y++) {
            [string appendFormat:@"%.2f ", mat.at<double>(x, y)];
        }
        [string appendFormat:@"\n"];
    }

    return string;
}


@end
