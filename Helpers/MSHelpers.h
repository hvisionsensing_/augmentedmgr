//
//  Created by Michał Mizera on 17.11.2013.
//


#import <Foundation/Foundation.h>


@interface MSHelpers : NSObject
+ (CGFloat)distanceBetweenPoints:(CGPoint)pointA pointB:(CGPoint)pointB;
@end
