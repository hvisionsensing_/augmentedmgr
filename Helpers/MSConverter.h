//
//  Created by Michał Mizera on 10.11.2013.
//


#import <Foundation/Foundation.h>


@interface MSConverter : NSObject

+ (NSDictionary *)dictFromPoint:(CGPoint)point;

+ (CGPoint)cgPointFromDict:(NSDictionary *)dictionary;
@end
