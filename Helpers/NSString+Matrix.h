//
//  Created by Michał Mizera on 09.10.2013.
//


#import <Foundation/Foundation.h>

@interface NSString (Matrix)
+ (NSString *)stringFromMat:(cv::Mat)mat;
@end
