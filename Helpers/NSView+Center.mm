//
//  Created by Michał Mizera on 15.11.2013.
//


#import "NSView+Center.h"


@implementation NSView (Center)


- (void)setCenter:(NSPoint)center {
    center.x -= NSWidth(self.frame) / 2.0f;
    center.y -= NSHeight(self.frame) / 2.0f;

    [self setFrameOrigin:center];
}

@end
